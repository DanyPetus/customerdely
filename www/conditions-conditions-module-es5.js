(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["conditions-conditions-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/conditions/conditions.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/conditions/conditions.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title>Terms & conditions</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg_img\" fullscreen>\n\t<div class=\"banner\">\n\t\t<img src=\"assets/img/logo.png\">\n\t</div>\n\n\t<div class=\"text_box\">\n\t\t<h2>Terms of use</h2>\n\t\t<p>\n\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.\n\t\t</p>\n\t</div>\n\t<div class=\"text_box\">\n\t\t<h2>Privacy Policy</h2>\n\t\t<p>\n\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\t\t</p>\n\t</div>\n\t<div class=\"text_box\">\n\t\t<h2>Terms of use</h2>\n\t\t<p>\n\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.\n\t\t</p>\n\t</div>\n\t<div class=\"text_box\">\n\t\t<h2>Privacy Policy</h2>\n\t\t<p>\n\t\t\tLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n\t\t</p>\n\t</div>\n</ion-content>"

/***/ }),

/***/ "./src/app/conditions/conditions.module.ts":
/*!*************************************************!*\
  !*** ./src/app/conditions/conditions.module.ts ***!
  \*************************************************/
/*! exports provided: ConditionsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionsPageModule", function() { return ConditionsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _conditions_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./conditions.page */ "./src/app/conditions/conditions.page.ts");







var routes = [
    {
        path: '',
        component: _conditions_page__WEBPACK_IMPORTED_MODULE_6__["ConditionsPage"]
    }
];
var ConditionsPageModule = /** @class */ (function () {
    function ConditionsPageModule() {
    }
    ConditionsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_conditions_page__WEBPACK_IMPORTED_MODULE_6__["ConditionsPage"]]
        })
    ], ConditionsPageModule);
    return ConditionsPageModule;
}());



/***/ }),

/***/ "./src/app/conditions/conditions.page.scss":
/*!*************************************************!*\
  !*** ./src/app/conditions/conditions.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat;\n  -webkit-backdrop-filter: saturate(120%) blur(10px);\n          backdrop-filter: saturate(120%) blur(10px); }\n  ion-content.bg_img::before {\n    content: '';\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background: var(--primary);\n    opacity: .2;\n    z-index: 1; }\n  ion-header::before {\n  content: '';\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  background: var(--primary);\n  opacity: .2;\n  z-index: 1; }\n  .banner {\n  padding-top: 26%;\n  padding-bottom: 26%;\n  position: relative;\n  z-index: 999; }\n  .banner img {\n    width: 139px;\n    display: block;\n    margin: 0 auto; }\n  .text_box {\n  background: var(--white);\n  border-radius: 10px;\n  padding: 20px 22px;\n  width: calc(100% - 25px);\n  margin: 0 auto;\n  margin-bottom: 13px;\n  padding-bottom: 7px;\n  position: relative;\n  z-index: 999; }\n  .text_box h2 {\n    margin: 0;\n    color: var(--text-dark);\n    font-size: 1.1rem;\n    font-weight: 500;\n    margin-bottom: 17px; }\n  .text_box p {\n    margin: 0;\n    color: var(--text-dark);\n    font-size: .95rem;\n    font-weight: 500;\n    padding-bottom: 13px;\n    line-height: 23px;\n    letter-spacing: .2px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29uZGl0aW9ucy9FOlxcaW9uaWM0XFxmb29kbWFydFxcY3VzdG9tZXIvc3JjXFxhcHBcXGNvbmRpdGlvbnNcXGNvbmRpdGlvbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWE7RUFDYixrREFBMEM7VUFBMUMsMENBQTBDLEVBQUE7RUFGM0M7SUFLRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixPQUFPO0lBQ1AsV0FBVztJQUNYLFlBQVk7SUFDWiwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFVBQVUsRUFBQTtFQU9aO0VBSUUsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sT0FBTztFQUNQLFdBQVc7RUFDWCxZQUFZO0VBQ1osMEJBQTBCO0VBQzFCLFdBQVc7RUFDWCxVQUFVLEVBQUE7RUFLWjtFQUNDLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDcEIsa0JBQWtCO0VBQ2pCLFlBQVksRUFBQTtFQUpiO0lBTUUsWUFBWTtJQUNaLGNBQWM7SUFDZCxjQUFjLEVBQUE7RUFJaEI7RUFDQyx3QkFBd0I7RUFDeEIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQix3QkFBd0I7RUFDeEIsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixtQkFBbUI7RUFFcEIsa0JBQWtCO0VBQ2pCLFlBQVksRUFBQTtFQVZiO0lBWUUsU0FBUztJQUNULHVCQUF1QjtJQUN2QixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLG1CQUFtQixFQUFBO0VBaEJyQjtJQW9CRSxTQUFTO0lBQ1QsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGlCQUFpQjtJQUNqQixvQkFBb0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbmRpdGlvbnMvY29uZGl0aW9ucy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5iZ19pbWcge1xyXG5cdC0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcucG5nKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuXHRiYWNrZHJvcC1maWx0ZXI6IHNhdHVyYXRlKDEyMCUpIGJsdXIoMTBweCk7XHJcblxyXG5cdCY6OmJlZm9yZSB7XHJcblx0XHRjb250ZW50OiAnJztcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdGhlaWdodDogMTAwJTtcclxuXHRcdGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0b3BhY2l0eTogLjI7XHJcblx0XHR6LWluZGV4OiAxO1xyXG5cclxuXHR9XHJcblxyXG5cclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcblx0Ly9iYWNrZHJvcC1maWx0ZXI6IHNhdHVyYXRlKDEyMCUpIGJsdXIoMTBweCk7XHJcblxyXG5cdCY6OmJlZm9yZSB7XHJcblx0XHRjb250ZW50OiAnJztcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHRvcDogMDtcclxuXHRcdGxlZnQ6IDA7XHJcblx0XHR3aWR0aDogMTAwJTtcclxuXHRcdGhlaWdodDogMTAwJTtcclxuXHRcdGJhY2tncm91bmQ6IHZhcigtLXByaW1hcnkpO1xyXG5cdFx0b3BhY2l0eTogLjI7XHJcblx0XHR6LWluZGV4OiAxO1xyXG5cclxuXHR9XHJcbn1cclxuXHJcbi5iYW5uZXIge1xyXG5cdHBhZGRpbmctdG9wOiAyNiU7XHJcblx0cGFkZGluZy1ib3R0b206IDI2JTtcclxucG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdHotaW5kZXg6IDk5OTtcclxuXHRpbWcge1xyXG5cdFx0d2lkdGg6IDEzOXB4O1xyXG5cdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRtYXJnaW46IDAgYXV0bztcclxuXHR9XHJcbn1cclxuXHJcbi50ZXh0X2JveCB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0td2hpdGUpO1xyXG5cdGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcblx0cGFkZGluZzogMjBweCAyMnB4O1xyXG5cdHdpZHRoOiBjYWxjKDEwMCUgLSAyNXB4KTtcclxuXHRtYXJnaW46IDAgYXV0bztcclxuXHRtYXJnaW4tYm90dG9tOiAxM3B4O1xyXG5cdHBhZGRpbmctYm90dG9tOiA3cHg7XHJcblxyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0ei1pbmRleDogOTk5O1xyXG5cdGgyIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0Zm9udC1zaXplOiAxLjFyZW07XHJcblx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMTdweDtcclxuXHR9XHJcblxyXG5cdHAge1xyXG5cdFx0bWFyZ2luOiAwO1xyXG5cdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRmb250LXNpemU6IC45NXJlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0XHRwYWRkaW5nLWJvdHRvbTogMTNweDtcclxuXHRcdGxpbmUtaGVpZ2h0OiAyM3B4O1xyXG5cdFx0bGV0dGVyLXNwYWNpbmc6IC4ycHg7XHJcblx0fVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/conditions/conditions.page.ts":
/*!***********************************************!*\
  !*** ./src/app/conditions/conditions.page.ts ***!
  \***********************************************/
/*! exports provided: ConditionsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConditionsPage", function() { return ConditionsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConditionsPage = /** @class */ (function () {
    function ConditionsPage() {
    }
    ConditionsPage.prototype.ngOnInit = function () {
    };
    ConditionsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-conditions',
            template: __webpack_require__(/*! raw-loader!./conditions.page.html */ "./node_modules/raw-loader/index.js!./src/app/conditions/conditions.page.html"),
            styles: [__webpack_require__(/*! ./conditions.page.scss */ "./src/app/conditions/conditions.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConditionsPage);
    return ConditionsPage;
}());



/***/ })

}]);
//# sourceMappingURL=conditions-conditions-module-es5.js.map