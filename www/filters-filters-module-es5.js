(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["filters-filters-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/filters/filters.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/filters/filters.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\n<ion-header>\n  <ion-toolbar>\n    <ion-title>filters</ion-title>\n  </ion-toolbar>\n</ion-header>\n-->\n\n<ion-content fullscreen>\n\n</ion-content>\n<ion-footer>\n\t<div class=\"container\">\n\t\t<div class=\"filter_box\">\n\t\t\t<ion-segment [(ngModel)]=\"tab\">\n\t\t\t\t<ion-segment-button value=\"sort_by\">\n\t\t\t\t\t<ion-label >\n\t\t\t\t\t\tSort by\n\t\t\t\t\t</ion-label>\n\t\t\t\t</ion-segment-button>\n\t\t\t\t<ion-segment-button value=\"price\">\n\t\t\t\t\t<ion-label class=\"active\">\n\t\t\t\t\t\tPrice\n\t\t\t\t\t\n\t\t\t\t\t</ion-label>\n\t\t\t\t</ion-segment-button>\n\t\t\t\t<ion-segment-button value=\"dietaty\">\n\t\t\t\t\t<ion-label>Dietaty</ion-label>\n\t\t\t\t</ion-segment-button>\n\t\t\t</ion-segment>\n\n\n\t\t\t<div class=\"'tab_content\" [ngSwitch]=\"tab\">\n\t\t\t\t<ion-list lines=\"none\" *ngSwitchCase=\"'sort_by'\">\n\t\t\t\t\t<ion-radio-group>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Recommended</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"1\"></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Best Rated</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"2\" checked></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Delivery Time</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"3\" ></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t</ion-radio-group>\n\t\t\t\t</ion-list>\t\n\t\t\t\t\n\t\t\t\t\n\t\t\t\t\n\t\t\t\t<ion-list lines=\"none\" *ngSwitchCase=\"'price'\">\n\t\t\t\t\t<ion-radio-group>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Low To Heigh</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"1\"></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Height To Low</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"2\" checked></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Delivery Time</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"3\" ></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t</ion-radio-group>\n\t\t\t\t</ion-list>\n\t\t\t\t \n\t\t\t\t\n\t\t\t\t\n\t\t\t\t<ion-list lines=\"none\" *ngSwitchCase=\"'dietaty'\">\n\t\t\t\t\t<ion-radio-group>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Recommended</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"1\"></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Best Rated</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"2\" checked></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t\t<ion-item>\n\t\t\t\t\t\t\t<ion-label>Delivery Time</ion-label>\n\t\t\t\t\t\t\t<ion-radio slot=\"end\" value=\"3\" ></ion-radio>\n\t\t\t\t\t\t</ion-item>\n\t\t\t\t\t</ion-radio-group>\n\t\t\t\t</ion-list>\n\t\t\t</div>\n\t\t</div>\n\t\t\n\t\t<ion-button expand=\"block\" class=\"btn ion-text-uppercase apply_filter\" (click)=\"dismiss()\">Apply Filter</ion-button>\n\t\t<ion-button expand=\"block\" class=\"btn ion-text-uppercase cancel\" (click)=\"dismiss()\">Cancel</ion-button>\n\t</div>\n</ion-footer>"

/***/ }),

/***/ "./src/app/filters/filters.module.ts":
/*!*******************************************!*\
  !*** ./src/app/filters/filters.module.ts ***!
  \*******************************************/
/*! exports provided: FiltersPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersPageModule", function() { return FiltersPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _filters_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./filters.page */ "./src/app/filters/filters.page.ts");







var routes = [
    {
        path: '',
        component: _filters_page__WEBPACK_IMPORTED_MODULE_6__["FiltersPage"]
    }
];
var FiltersPageModule = /** @class */ (function () {
    function FiltersPageModule() {
    }
    FiltersPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_filters_page__WEBPACK_IMPORTED_MODULE_6__["FiltersPage"]]
        })
    ], FiltersPageModule);
    return FiltersPageModule;
}());



/***/ }),

/***/ "./src/app/filters/filters.page.scss":
/*!*******************************************!*\
  !*** ./src/app/filters/filters.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  --background: rgba(0, 0, 1, 0.58) !important;\n  --padding-bottom: 0 !important; }\n\nion-footer {\n  --border-color: var(--transparent) !important; }\n\nion-footer .container {\n    width: calc(100% - 48px);\n    margin: 0 auto;\n    overflow: hidden; }\n\nion-footer .container .filter_box {\n      background: var(--white);\n      border-radius: 7px 7px 0 0px; }\n\nion-footer .container .filter_box ion-segment {\n        border: none !important;\n        position: relative;\n        min-height: 56px; }\n\nion-footer .container .filter_box ion-segment ion-segment-button {\n          background: none !important;\n          position: relative;\n          font-size: 1.2rem;\n          font-weight: 400;\n          color: #d0d0d0 !important;\n          text-transform: unset !important;\n          letter-spacing: 0;\n          --border-color: var(--transparent) !important; }\n\nion-footer .container .filter_box ion-segment ion-segment-button ion-label {\n            position: relative;\n            padding: 0 8px; }\n\nion-footer .container .filter_box ion-segment ion-segment-button ion-label.active::before {\n              content: '';\n              position: absolute;\n              top: 0;\n              right: 0;\n              width: 7px;\n              height: 7px;\n              border-radius: 50%;\n              background: var(--secondary);\n              z-index: 99;\n              opacity: .5; }\n\nion-footer .container .filter_box ion-segment ion-segment-button::before {\n            content: '';\n            position: absolute;\n            bottom: 0px;\n            right: 0;\n            left: 0;\n            margin: 0 auto;\n            height: 4px;\n            -webkit-transition: all .3s;\n            transition: all .3s;\n            width: 0;\n            background: var(--black);\n            border-radius: 50px; }\n\nion-footer .container .filter_box ion-segment ion-segment-button.segment-button-checked {\n            color: var(--text-dark) !important;\n            font-weight: 500; }\n\nion-footer .container .filter_box ion-segment ion-segment-button.segment-button-checked ion-label.active::before {\n              opacity: 1; }\n\nion-footer .container .filter_box ion-segment ion-segment-button.segment-button-checked::before {\n              width: 75px;\n              -webkit-transition: all .3s;\n              transition: all .3s; }\n\nion-footer .container .filter_box ion-segment ion-segment-button:host(.activated), ion-footer .container .filter_box ion-segment ion-segment-button:host(.segment-button-checked) {\n            --border-color: var(--transparent) !important; }\n\nion-footer .container .filter_box ion-item {\n        --inner-padding-end: 0px;\n        --inner-min-height: unset !important;\n        min-height: unset;\n        --padding-start: 0;\n        --background: var(--white);\n        width: calc(100% - 0px);\n        margin: 0 auto;\n        overflow: hidden;\n        padding: 0px 17px;\n        background: var(--white);\n        min-height: unset; }\n\nion-footer .container .filter_box ion-item ion-label {\n          margin: 0;\n          color: var(--text-dark);\n          text-align: center;\n          font-weight: 400;\n          font-size: 1.15rem; }\n\nion-footer .container .filter_box ion-item ion-label .img_box {\n            width: 61px; }\n\nion-footer .container .filter_box ion-item ion-label .img_box img {\n              width: 54px; }\n\nion-footer .container .filter_box ion-item ion-label h2 {\n            color: var(--black);\n            font-weight: 500;\n            font-size: .95rem;\n            margin: 0 18px; }\n\nion-footer .container .filter_box ion-item ion-radio {\n          margin-top: 0;\n          margin-bottom: 0;\n          --color: transparent;\n          --color-checked: transparent;\n          position: absolute;\n          top: 0;\n          left: 0;\n          width: 100%;\n          height: 100%; }\n\nion-footer .container .filter_box ion-item ion-radio .radio-icon {\n            display: none; }\n\nion-footer .container .filter_box ion-item.item-radio-checked ion-label {\n          color: var(--primary);\n          font-weight: 700; }\n\nion-footer .container .button.btn {\n      --border-radius: 7px;\n      margin: 0;\n      margin-bottom: 10px;\n      font-size: 1rem;\n      font-weight: 500; }\n\nion-footer .container .button.btn.apply_filter {\n        --background: var(--primary) !important;\n        color: var(--white) !important;\n        --border-radius: 0px 0px 7px 7px; }\n\nion-footer .container .button.btn.cancel {\n        --background: var(--white) !important;\n        color: var(--primary) !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmlsdGVycy9FOlxcaW9uaWM0XFxmb29kbWFydFxcY3VzdG9tZXIvc3JjXFxhcHBcXGZpbHRlcnNcXGZpbHRlcnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNENBQWE7RUFDZCw4QkFBaUIsRUFBQTs7QUFJbEI7RUFDQyw2Q0FBZSxFQUFBOztBQURoQjtJQUlFLHdCQUF3QjtJQUN4QixjQUFjO0lBQ2QsZ0JBQWdCLEVBQUE7O0FBTmxCO01BU0csd0JBQXdCO01BQ3hCLDRCQUE0QixFQUFBOztBQVYvQjtRQWFJLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsZ0JBQWdCLEVBQUE7O0FBZnBCO1VBa0JLLDJCQUEyQjtVQUMzQixrQkFBa0I7VUFDbEIsaUJBQWlCO1VBQ2pCLGdCQUFnQjtVQUNoQix5QkFBeUI7VUFDekIsZ0NBQWdDO1VBQ2hDLGlCQUFpQjtVQUNqQiw2Q0FBZSxFQUFBOztBQXpCcEI7WUE0Qk0sa0JBQWtCO1lBQ2xCLGNBQWMsRUFBQTs7QUE3QnBCO2NBaUNRLFdBQVc7Y0FDWCxrQkFBa0I7Y0FDbEIsTUFBTTtjQUNOLFFBQVE7Y0FDUixVQUFVO2NBQ1YsV0FBVztjQUNYLGtCQUFrQjtjQUNsQiw0QkFBNEI7Y0FDNUIsV0FBVztjQUNYLFdBQVcsRUFBQTs7QUExQ25CO1lBZ0RNLFdBQVc7WUFDWCxrQkFBa0I7WUFDbEIsV0FBVztZQUNYLFFBQVE7WUFDUixPQUFPO1lBQ1AsY0FBYztZQUNkLFdBQVc7WUFDWCwyQkFBbUI7WUFBbkIsbUJBQW1CO1lBQ25CLFFBQVE7WUFDUix3QkFBd0I7WUFDeEIsbUJBQW1CLEVBQUE7O0FBMUR6QjtZQThETSxrQ0FBa0M7WUFDbEMsZ0JBQWdCLEVBQUE7O0FBL0R0QjtjQW9FUyxVQUFVLEVBQUE7O0FBcEVuQjtjQTBFTyxXQUFXO2NBQ1gsMkJBQW1CO2NBQW5CLG1CQUFtQixFQUFBOztBQTNFMUI7WUFpRk0sNkNBQWUsRUFBQTs7QUFqRnJCO1FBMEZJLHdCQUFvQjtRQUNwQixvQ0FBbUI7UUFDbkIsaUJBQWlCO1FBQ2pCLGtCQUFnQjtRQUNoQiwwQkFBYTtRQUNiLHVCQUF1QjtRQUN2QixjQUFjO1FBQ2QsZ0JBQWdCO1FBQ2hCLGlCQUFpQjtRQUNqQix3QkFBd0I7UUFDeEIsaUJBQWlCLEVBQUE7O0FBcEdyQjtVQXVHSyxTQUFTO1VBQ1QsdUJBQXVCO1VBQ3ZCLGtCQUFrQjtVQUNsQixnQkFBZ0I7VUFDaEIsa0JBQWtCLEVBQUE7O0FBM0d2QjtZQThHTSxXQUFXLEVBQUE7O0FBOUdqQjtjQWlITyxXQUFXLEVBQUE7O0FBakhsQjtZQXNITSxtQkFBbUI7WUFDbkIsZ0JBQWdCO1lBQ2hCLGlCQUFpQjtZQUNqQixjQUFjLEVBQUE7O0FBekhwQjtVQStISyxhQUFhO1VBQ2IsZ0JBQWdCO1VBQ2hCLG9CQUFRO1VBQ1IsNEJBQWdCO1VBQ2hCLGtCQUFrQjtVQUNsQixNQUFNO1VBQ04sT0FBTztVQUNQLFdBQVc7VUFDWCxZQUFZLEVBQUE7O0FBdklqQjtZQTBJTSxhQUFhLEVBQUE7O0FBMUluQjtVQWdKTSxxQkFBcUI7VUFDckIsZ0JBQWdCLEVBQUE7O0FBakp0QjtNQXlKRyxvQkFBZ0I7TUFDaEIsU0FBUztNQUNULG1CQUFtQjtNQUNuQixlQUFlO01BQ2YsZ0JBQWdCLEVBQUE7O0FBN0puQjtRQWdLSSx1Q0FBYTtRQUNiLDhCQUE4QjtRQUM5QixnQ0FBZ0IsRUFBQTs7QUFsS3BCO1FBc0tJLHFDQUFhO1FBQ2IsZ0NBQWdDLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9maWx0ZXJzL2ZpbHRlcnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gIC0tYmFja2dyb3VuZDogcmdiYSgwLCAwLCAxLCAwLjU4KSAhaW1wb3J0YW50OyBcclxuXHQtLXBhZGRpbmctYm90dG9tOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuIFxyXG5cclxuaW9uLWZvb3RlciB7XHJcblx0LS1ib3JkZXItY29sb3I6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xyXG5cclxuXHQuY29udGFpbmVyIHtcclxuXHRcdHdpZHRoOiBjYWxjKDEwMCUgLSA0OHB4KTtcclxuXHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHJcblx0XHQuZmlsdGVyX2JveCB7XHJcblx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0Ym9yZGVyLXJhZGl1czogN3B4IDdweCAwIDBweDtcclxuXHJcblx0XHRcdGlvbi1zZWdtZW50IHtcclxuXHRcdFx0XHRib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcclxuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0bWluLWhlaWdodDogNTZweDtcclxuXHJcblx0XHRcdFx0aW9uLXNlZ21lbnQtYnV0dG9uIHtcclxuXHRcdFx0XHRcdGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDQwMDtcclxuXHRcdFx0XHRcdGNvbG9yOiAjZDBkMGQwICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHR0ZXh0LXRyYW5zZm9ybTogdW5zZXQgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdGxldHRlci1zcGFjaW5nOiAwO1xyXG5cdFx0XHRcdFx0LS1ib3JkZXItY29sb3I6IHZhcigtLXRyYW5zcGFyZW50KSAhaW1wb3J0YW50O1xyXG5cclxuXHRcdFx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0XHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdFx0XHRcdFx0cGFkZGluZzogMCA4cHg7XHJcblxyXG5cdFx0XHRcdFx0XHQmLmFjdGl2ZSB7XHJcblx0XHRcdFx0XHRcdFx0Jjo6YmVmb3JlIHtcclxuXHRcdFx0XHRcdFx0XHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0XHRcdFx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRcdFx0XHRcdFx0dG9wOiAwO1xyXG5cdFx0XHRcdFx0XHRcdFx0cmlnaHQ6IDA7XHJcblx0XHRcdFx0XHRcdFx0XHR3aWR0aDogN3B4O1xyXG5cdFx0XHRcdFx0XHRcdFx0aGVpZ2h0OiA3cHg7XHJcblx0XHRcdFx0XHRcdFx0XHRib3JkZXItcmFkaXVzOiA1MCU7XHJcblx0XHRcdFx0XHRcdFx0XHRiYWNrZ3JvdW5kOiB2YXIoLS1zZWNvbmRhcnkpO1xyXG5cdFx0XHRcdFx0XHRcdFx0ei1pbmRleDogOTk7XHJcblx0XHRcdFx0XHRcdFx0XHRvcGFjaXR5OiAuNTtcclxuXHRcdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHQmOjpiZWZvcmUge1xyXG5cdFx0XHRcdFx0XHRjb250ZW50OiAnJztcclxuXHRcdFx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRcdFx0XHRib3R0b206IDBweDtcclxuXHRcdFx0XHRcdFx0cmlnaHQ6IDA7XHJcblx0XHRcdFx0XHRcdGxlZnQ6IDA7XHJcblx0XHRcdFx0XHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdFx0XHRcdFx0XHRoZWlnaHQ6IDRweDtcclxuXHRcdFx0XHRcdFx0dHJhbnNpdGlvbjogYWxsIC4zcztcclxuXHRcdFx0XHRcdFx0d2lkdGg6IDA7XHJcblx0XHRcdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWJsYWNrKTtcclxuXHRcdFx0XHRcdFx0Ym9yZGVyLXJhZGl1czogNTBweDtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0XHQmLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQge1xyXG5cdFx0XHRcdFx0XHRjb2xvcjogdmFyKC0tdGV4dC1kYXJrKSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cclxuXHRcdFx0XHRcdFx0aW9uLWxhYmVsIHtcclxuXHRcdFx0XHRcdFx0XHQmLmFjdGl2ZSB7XHJcblx0XHRcdFx0XHRcdFx0XHQmOjpiZWZvcmUge1xyXG5cdFx0XHRcdFx0XHRcdFx0XHRvcGFjaXR5OiAxO1xyXG5cdFx0XHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdFx0Jjo6YmVmb3JlIHtcclxuXHRcdFx0XHRcdFx0XHR3aWR0aDogNzVweDtcclxuXHRcdFx0XHRcdFx0XHR0cmFuc2l0aW9uOiBhbGwgLjNzO1xyXG5cdFx0XHRcdFx0XHR9XHJcblx0XHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdFx0Jjpob3N0KC5hY3RpdmF0ZWQpLFxyXG5cdFx0XHRcdFx0Jjpob3N0KC5zZWdtZW50LWJ1dHRvbi1jaGVja2VkKSB7XHJcblx0XHRcdFx0XHRcdC0tYm9yZGVyLWNvbG9yOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fVxyXG5cclxuXHJcblx0XHRcdGlvbi1pdGVtIHtcclxuXHRcdFx0XHQtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcblx0XHRcdFx0LS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdG1pbi1oZWlnaHQ6IHVuc2V0O1xyXG5cdFx0XHRcdC0tcGFkZGluZy1zdGFydDogMDtcclxuXHRcdFx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHR3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcclxuXHRcdFx0XHRtYXJnaW46IDAgYXV0bztcclxuXHRcdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0XHRcdHBhZGRpbmc6IDBweCAxN3B4O1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHRtaW4taGVpZ2h0OiB1bnNldDtcclxuXHJcblx0XHRcdFx0aW9uLWxhYmVsIHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDQwMDtcclxuXHRcdFx0XHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHJcblx0XHRcdFx0XHQuaW1nX2JveCB7XHJcblx0XHRcdFx0XHRcdHdpZHRoOiA2MXB4O1xyXG5cclxuXHRcdFx0XHRcdFx0aW1nIHtcclxuXHRcdFx0XHRcdFx0XHR3aWR0aDogNTRweDtcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHRcdGgyIHtcclxuXHRcdFx0XHRcdFx0Y29sb3I6IHZhcigtLWJsYWNrKTtcclxuXHRcdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDUwMDtcclxuXHRcdFx0XHRcdFx0Zm9udC1zaXplOiAuOTVyZW07XHJcblx0XHRcdFx0XHRcdG1hcmdpbjogMCAxOHB4O1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aW9uLXJhZGlvIHtcclxuXHRcdFx0XHRcdC8vZGlzcGxheTogbm9uZTtcclxuXHRcdFx0XHRcdG1hcmdpbi10b3A6IDA7XHJcblx0XHRcdFx0XHRtYXJnaW4tYm90dG9tOiAwO1xyXG5cdFx0XHRcdFx0LS1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcblx0XHRcdFx0XHQtLWNvbG9yLWNoZWNrZWQ6IHRyYW5zcGFyZW50O1xyXG5cdFx0XHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRcdFx0dG9wOiAwO1xyXG5cdFx0XHRcdFx0bGVmdDogMDtcclxuXHRcdFx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRcdFx0aGVpZ2h0OiAxMDAlO1xyXG5cclxuXHRcdFx0XHRcdC5yYWRpby1pY29uIHtcclxuXHRcdFx0XHRcdFx0ZGlzcGxheTogbm9uZTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblxyXG5cdFx0XHRcdCYuaXRlbS1yYWRpby1jaGVja2VkIHtcclxuXHRcdFx0XHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdFx0XHRcdGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcclxuXHRcdFx0XHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0LmJ1dHRvbi5idG4ge1xyXG5cdFx0XHQtLWJvcmRlci1yYWRpdXM6IDdweDtcclxuXHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cdFx0XHRmb250LXNpemU6IDFyZW07XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblxyXG5cdFx0XHQmLmFwcGx5X2ZpbHRlciB7XHJcblx0XHRcdFx0LS1iYWNrZ3JvdW5kOiB2YXIoLS1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSkgIWltcG9ydGFudDtcclxuXHRcdFx0XHQtLWJvcmRlci1yYWRpdXM6IDBweCAwcHggN3B4IDdweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Ji5jYW5jZWwge1xyXG5cdFx0XHRcdC0tYmFja2dyb3VuZDogdmFyKC0td2hpdGUpICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXByaW1hcnkpICFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHJcblx0XHR9XHJcblx0fVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/filters/filters.page.ts":
/*!*****************************************!*\
  !*** ./src/app/filters/filters.page.ts ***!
  \*****************************************/
/*! exports provided: FiltersPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersPage", function() { return FiltersPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var FiltersPage = /** @class */ (function () {
    function FiltersPage(modalController) {
        this.modalController = modalController;
        this.tab = "sort_by";
    }
    FiltersPage.prototype.ngOnInit = function () {
    };
    FiltersPage.prototype.dismiss = function () {
        this.modalController.dismiss();
    };
    FiltersPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] }
    ]; };
    FiltersPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-filters',
            template: __webpack_require__(/*! raw-loader!./filters.page.html */ "./node_modules/raw-loader/index.js!./src/app/filters/filters.page.html"),
            styles: [__webpack_require__(/*! ./filters.page.scss */ "./src/app/filters/filters.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]])
    ], FiltersPage);
    return FiltersPage;
}());



/***/ })

}]);
//# sourceMappingURL=filters-filters-module-es5.js.map