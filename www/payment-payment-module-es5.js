(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["payment-payment-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/payment/payment.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/payment/payment.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>Select Payment Method</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg_img\" fullscreen>\n\t<ion-list lines=\"none\">\n\t\t<ion-radio-group>\n\t\t\t<ion-item>\n\t\t\t\t<ion-label class=\"d-flex\">\n\t\t\t\t\t<div class=\"img_box\">\n\t\t\t\t\t\t<img src=\"assets/img/payu.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<h2>PayU Money</h2>\n\t\t\t\t</ion-label>\n\t\t\t\t<ion-radio slot=\"end\" value=\"1\" checked></ion-radio>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<ion-label class=\"d-flex\">\n\t\t\t\t\t<div class=\"img_box\">\n\t\t\t\t\t\t<img src=\"assets/img/paypal.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<h2>PayPal Money</h2>\n\t\t\t\t</ion-label>\n\t\t\t\t<ion-radio slot=\"end\" value=\"2\"></ion-radio>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t\t<ion-label class=\"d-flex\">\n\t\t\t\t\t<div class=\"img_box\">\n\t\t\t\t\t\t<img src=\"assets/img/card.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<h2>Credit / Debit Card</h2>\n\t\t\t\t</ion-label>\n\t\t\t\t<ion-radio slot=\"end\" value=\"3\"></ion-radio>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t\t<ion-label class=\"d-flex\">\n\t\t\t\t\t<div class=\"img_box\">\n\t\t\t\t\t\t<img src=\"assets/img/cod.png\">\n\t\t\t\t\t</div>\n\t\t\t\t\t<h2>Cash on Delivery</h2>\n\t\t\t\t</ion-label>\n\t\t\t\t<ion-radio slot=\"end\" value=\"4\"></ion-radio>\n\t\t\t</ion-item>\n\t\t</ion-radio-group>\n\t</ion-list>\n</ion-content>\n\n<ion-footer>\n\t<div class=\"amount\">\n\t\t<h2 class=\"d-flex\">Amount Payable <span class=\"end\">$25.50</span></h2>\n\t</div>\n\n\t<ion-button expand=\"full\" no class=\"btn ion-text-uppercase ion-no-margin\" (click)=\"continue()\">\n\t\tProceed To Checkout\n\t\t<ion-icon class=\"zmdi zmdi-chevron-right ion-text-end\"></ion-icon>\n\t</ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/payment/payment.module.ts":
/*!*******************************************!*\
  !*** ./src/app/payment/payment.module.ts ***!
  \*******************************************/
/*! exports provided: PaymentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentPageModule", function() { return PaymentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _payment_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./payment.page */ "./src/app/payment/payment.page.ts");







var routes = [
    {
        path: '',
        component: _payment_page__WEBPACK_IMPORTED_MODULE_6__["PaymentPage"]
    }
];
var PaymentPageModule = /** @class */ (function () {
    function PaymentPageModule() {
    }
    PaymentPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_payment_page__WEBPACK_IMPORTED_MODULE_6__["PaymentPage"]]
        })
    ], PaymentPageModule);
    return PaymentPageModule;
}());



/***/ }),

/***/ "./src/app/payment/payment.page.scss":
/*!*******************************************!*\
  !*** ./src/app/payment/payment.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat;\n  --padding: 0 !important; }\n  ion-content.bg_img::before {\n    content: '';\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background: var(--primary);\n    opacity: .2;\n    z-index: 1; }\n  ion-header {\n  -webkit-backdrop-filter: saturate(120%) blur(10px);\n          backdrop-filter: saturate(120%) blur(10px);\n  margin-bottom: 8px; }\n  ion-header::before {\n    content: '';\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background: var(--primary);\n    opacity: .2; }\n  ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  margin-top: 10px;\n  overflow: hidden;\n  position: relative;\n  z-index: 99; }\n  ion-list ion-item {\n    --inner-padding-end: 0px;\n    --inner-min-height: unset !important;\n    min-height: unset;\n    --padding-start: 0;\n    --background: var(--white);\n    width: calc(100% - 30px);\n    margin: 0 auto;\n    overflow: hidden;\n    border-radius: 9px;\n    margin-bottom: 10px;\n    padding: 6px 17px;\n    background: var(--white);\n    min-height: unset; }\n  ion-list ion-item ion-label {\n      margin: 0; }\n  ion-list ion-item ion-label .img_box {\n        width: 61px; }\n  ion-list ion-item ion-label .img_box img {\n          width: 54px; }\n  ion-list ion-item ion-label h2 {\n        color: var(--black);\n        font-weight: 500;\n        font-size: .95rem;\n        margin: 0 18px; }\n  ion-list ion-item ion-radio {\n      margin-top: 0;\n      margin-bottom: 0;\n      --color-checked: var(--secondary); }\n  ion-footer {\n  background: var(--white);\n  border-radius: 10px 10px 0 0px;\n  padding-top: 15px; }\n  ion-footer .amount h2 {\n    margin: 0;\n    color: var(--primary);\n    font-weight: 700;\n    font-size: 1.2rem;\n    letter-spacing: 0;\n    width: calc(100% - 48px);\n    margin: 0 auto;\n    margin-bottom: 11px; }\n  ion-footer .amount h2 span {\n      color: #000; }\n  ion-footer .button.btn {\n    font-size: 1.15rem;\n    font-weight: 800;\n    height: 66px; }\n  ion-footer .button.btn ion-icon {\n      font-size: 2rem;\n      min-width: 41px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGF5bWVudC9FOlxcaW9uaWM0XFxmb29kbWFydFxcY3VzdG9tZXIvc3JjXFxhcHBcXHBheW1lbnRcXHBheW1lbnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWE7RUFDYix1QkFBVSxFQUFBO0VBRlg7SUFNRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixPQUFPO0lBQ1AsV0FBVztJQUNYLFlBQVk7SUFDWiwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFVBQVUsRUFBQTtFQUtaO0VBQ0Msa0RBQTBDO1VBQTFDLDBDQUEwQztFQUMxQyxrQkFBa0IsRUFBQTtFQUZuQjtJQUtFLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxXQUFXO0lBQ1gsWUFBWTtJQUNaLDBCQUEwQjtJQUMxQixXQUFXLEVBQUE7RUFLYjtFQUNDLHlDQUF5QztFQUN6QyxTQUFTO0VBQ1QsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsa0JBQWtCO0VBQ2xCLFdBQVcsRUFBQTtFQVBaO0lBVUUsd0JBQW9CO0lBQ3BCLG9DQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsa0JBQWdCO0lBQ2hCLDBCQUFhO0lBQ2Isd0JBQXdCO0lBQ3hCLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsd0JBQXdCO0lBQ3hCLGlCQUFpQixFQUFBO0VBdEJuQjtNQXlCRyxTQUFTLEVBQUE7RUF6Qlo7UUE0QkksV0FBVyxFQUFBO0VBNUJmO1VBK0JLLFdBQVcsRUFBQTtFQS9CaEI7UUFvQ0ksbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixpQkFBaUI7UUFDakIsY0FBYyxFQUFBO0VBdkNsQjtNQTRDRyxhQUFhO01BQ2IsZ0JBQWdCO01BQ2hCLGlDQUFnQixFQUFBO0VBS25CO0VBQ0Msd0JBQXdCO0VBQ3hCLDhCQUE4QjtFQUM5QixpQkFBaUIsRUFBQTtFQUhsQjtJQU9HLFNBQVM7SUFDVCxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsd0JBQXdCO0lBQ3hCLGNBQWM7SUFDZCxtQkFBbUIsRUFBQTtFQWR0QjtNQWlCSSxXQUFXLEVBQUE7RUFqQmY7SUF1QkUsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZLEVBQUE7RUF6QmQ7TUE0QkcsZUFBZTtNQUNmLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BheW1lbnQvcGF5bWVudC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5iZ19pbWcge1xyXG5cdC0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcucG5nKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuXHQtLXBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuXHJcblx0Ly8tLXBhZGRpbmctdG9wOiAwICFpbXBvcnRhbnQ7XHJcblx0Jjo6YmVmb3JlIHtcclxuXHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0dG9wOiAwO1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiAxMDAlO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRvcGFjaXR5OiAuMjtcclxuXHRcdHotaW5kZXg6IDE7XHJcblxyXG5cdH1cclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcblx0YmFja2Ryb3AtZmlsdGVyOiBzYXR1cmF0ZSgxMjAlKSBibHVyKDEwcHgpO1xyXG5cdG1hcmdpbi1ib3R0b206IDhweDtcclxuXHJcblx0Jjo6YmVmb3JlIHtcclxuXHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0dG9wOiAwO1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiAxMDAlO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRvcGFjaXR5OiAuMjtcclxuXHJcblx0fVxyXG59XHJcblxyXG5pb24tbGlzdCB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0bWFyZ2luOiAwO1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHR6LWluZGV4OiA5OTtcclxuXHJcblx0aW9uLWl0ZW0ge1xyXG5cdFx0LS1pbm5lci1wYWRkaW5nLWVuZDogMHB4O1xyXG5cdFx0LS1pbm5lci1taW4taGVpZ2h0OiB1bnNldCAhaW1wb3J0YW50O1xyXG5cdFx0bWluLWhlaWdodDogdW5zZXQ7XHJcblx0XHQtLXBhZGRpbmctc3RhcnQ6IDA7XHJcblx0XHQtLWJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdHdpZHRoOiBjYWxjKDEwMCUgLSAzMHB4KTtcclxuXHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDlweDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDEwcHg7XHJcblx0XHRwYWRkaW5nOiA2cHggMTdweDtcclxuXHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdG1pbi1oZWlnaHQ6IHVuc2V0O1xyXG5cclxuXHRcdGlvbi1sYWJlbCB7XHJcblx0XHRcdG1hcmdpbjogMDtcclxuXHJcblx0XHRcdC5pbWdfYm94IHtcclxuXHRcdFx0XHR3aWR0aDogNjFweDtcclxuXHJcblx0XHRcdFx0aW1nIHtcclxuXHRcdFx0XHRcdHdpZHRoOiA1NHB4O1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aDIge1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS1ibGFjayk7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDUwMDtcclxuXHRcdFx0XHRmb250LXNpemU6IC45NXJlbTtcclxuXHRcdFx0XHRtYXJnaW46IDAgMThweDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdGlvbi1yYWRpbyB7XHJcblx0XHRcdG1hcmdpbi10b3A6IDA7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDA7XHJcblx0XHRcdC0tY29sb3ItY2hlY2tlZDogdmFyKC0tc2Vjb25kYXJ5KTtcclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbmlvbi1mb290ZXIge1xyXG5cdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRib3JkZXItcmFkaXVzOiAxMHB4IDEwcHggMCAwcHg7XHJcblx0cGFkZGluZy10b3A6IDE1cHg7XHJcblxyXG5cdC5hbW91bnQge1xyXG5cdFx0aDIge1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS1wcmltYXJ5KTtcclxuXHRcdFx0Zm9udC13ZWlnaHQ6IDcwMDtcclxuXHRcdFx0Zm9udC1zaXplOiAxLjJyZW07XHJcblx0XHRcdGxldHRlci1zcGFjaW5nOiAwO1xyXG5cdFx0XHR3aWR0aDogY2FsYygxMDAlIC0gNDhweCk7XHJcblx0XHRcdG1hcmdpbjogMCBhdXRvO1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAxMXB4O1xyXG5cclxuXHRcdFx0c3BhbiB7XHJcblx0XHRcdFx0Y29sb3I6ICMwMDA7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdC5idXR0b24uYnRuIHtcclxuXHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA4MDA7XHJcblx0XHRoZWlnaHQ6IDY2cHg7XHJcblxyXG5cdFx0aW9uLWljb24ge1xyXG5cdFx0XHRmb250LXNpemU6IDJyZW07XHJcblx0XHRcdG1pbi13aWR0aDogNDFweDtcclxuXHRcdH1cclxuXHR9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/payment/payment.page.ts":
/*!*****************************************!*\
  !*** ./src/app/payment/payment.page.ts ***!
  \*****************************************/
/*! exports provided: PaymentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentPage", function() { return PaymentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var PaymentPage = /** @class */ (function () {
    function PaymentPage(route) {
        this.route = route;
    }
    PaymentPage.prototype.ngOnInit = function () {
    };
    PaymentPage.prototype.continue = function () {
        this.route.navigate(['./order-confirmed']);
    };
    PaymentPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    PaymentPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-payment',
            template: __webpack_require__(/*! raw-loader!./payment.page.html */ "./node_modules/raw-loader/index.js!./src/app/payment/payment.page.html"),
            styles: [__webpack_require__(/*! ./payment.page.scss */ "./src/app/payment/payment.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], PaymentPage);
    return PaymentPage;
}());



/***/ })

}]);
//# sourceMappingURL=payment-payment-module-es5.js.map