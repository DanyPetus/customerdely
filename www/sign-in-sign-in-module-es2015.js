(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sign-in-sign-in-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/sign-in/sign-in.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/sign-in/sign-in.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar >\n\t\t<ion-title>Sign in <span class=\"ion-text-uppercase end ion-float-end\" (click)=\"goTosignup()\">Sign Up Now</span>\n\t\t</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg_img\" fullscreen>\n\t<div class=\"banner\">\n\t\t<img src=\"assets/img/logo.png\">\n\t</div>\n\t<h2 text-center>Sign in now</h2>\n\t<div class=\"form\">\n\t\t<div class=\"list\">\n\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Email Address</ion-label>\n\t\t\t\t<ion-input type=\"email\" value=\"samanthasmith@mail.com\"></ion-input>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Password</ion-label>\n\t\t\t\t<ion-input type=\"password\" value=\"123456\"></ion-input>\n\t\t\t\t<h3 slot=\"end\">Forgot?</h3>\n\t\t\t</ion-item>\n\n\t\t\t<ion-button expand=\"block\" class=\"btn ion-text-uppercase\" (click)=\"goTohome()\">Sign in</ion-button>\n\t\t</div>\n\t</div>\n\n\t<div class=\"or_continue_with\">\n\t\t<h2 text-center>Or Continue with</h2>\n\t\t<ion-row>\n\t\t\t<ion-col size=\"6\">\n\t\t\t\t<ion-button expand=\"block\" fill=\"outline\" class=\"btn ion-text-capitalize\" (click)=\"goTohome()\"> \n\t\t\t\t\t<ion-icon slot=\"start\" class=\"zmdi zmdi-facebook\"></ion-icon>\n\t\t\t\t\tFacebook\n\t\t\t\t</ion-button>\n\t\t\t</ion-col>\n\n\t\t\t<ion-col size=\"6\">\n\t\t\t\t<ion-button expand=\"block\" fill=\"outline\" class=\"btn ion-text-capitalize\" (click)=\"goTohome()\">\n\t\t\t\t\t<ion-icon slot=\"start\" class=\"zmdi zmdi-google\"></ion-icon>\n\t\t\t\t\tGoogle\n\t\t\t\t</ion-button>\n\t\t\t</ion-col>\n\t\t</ion-row>\n\t</div>\n</ion-content>"

/***/ }),

/***/ "./src/app/sign-in/sign-in.module.ts":
/*!*******************************************!*\
  !*** ./src/app/sign-in/sign-in.module.ts ***!
  \*******************************************/
/*! exports provided: SignInPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInPageModule", function() { return SignInPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sign_in_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sign-in.page */ "./src/app/sign-in/sign-in.page.ts");







const routes = [
    {
        path: '',
        component: _sign_in_page__WEBPACK_IMPORTED_MODULE_6__["SignInPage"]
    }
];
let SignInPageModule = class SignInPageModule {
};
SignInPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_sign_in_page__WEBPACK_IMPORTED_MODULE_6__["SignInPage"]]
    })
], SignInPageModule);



/***/ }),

/***/ "./src/app/sign-in/sign-in.page.scss":
/*!*******************************************!*\
  !*** ./src/app/sign-in/sign-in.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat; }\n\nion-header ion-toolbar ion-title .end {\n  color: var(--secondary);\n  letter-spacing: .5px; }\n\n.banner {\n  padding-top: 26%;\n  padding-bottom: 26%; }\n\n.banner img {\n    width: 139px;\n    display: block;\n    margin: 0 auto; }\n\nh2 {\n  margin: 0;\n  color: var(--primary-text);\n  font-size: 1.1rem;\n  font-weight: 500;\n  margin-bottom: 40px; }\n\n.or_continue_with {\n  display: block;\n  margin: 0 auto;\n  width: calc(100% - 43px);\n  margin-top: 15%; }\n\n.or_continue_with h2 {\n    margin-bottom: 23px; }\n\n.or_continue_with ion-row {\n    margin: 0 -5px; }\n\n.or_continue_with ion-row ion-col .button.btn.button-outline {\n      font-weight: 400;\n      font-size: .95rem; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbi1pbi9FOlxcaW9uaWM0XFxmb29kbWFydFxcY3VzdG9tZXIvc3JjXFxhcHBcXHNpZ24taW5cXHNpZ24taW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWEsRUFBQTs7QUFJZDtFQUNDLHVCQUF1QjtFQUN2QixvQkFBb0IsRUFBQTs7QUFHckI7RUFDQyxnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7O0FBRnBCO0lBS0UsWUFBWTtJQUNaLGNBQWM7SUFDZCxjQUFjLEVBQUE7O0FBSWhCO0VBQ0MsU0FBUztFQUNULDBCQUEwQjtFQUMxQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBOztBQUdwQjtFQUNDLGNBQWM7RUFDZCxjQUFjO0VBQ2Qsd0JBQXdCO0VBQ3hCLGVBQWUsRUFBQTs7QUFKaEI7SUFPRSxtQkFBbUIsRUFBQTs7QUFQckI7SUFXRSxjQUFjLEVBQUE7O0FBWGhCO01BZUksZ0JBQWdCO01BQ2hCLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2lnbi1pbi9zaWduLWluLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50LmJnX2ltZyB7XHJcblx0LS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9iZy5wbmcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xyXG5cclxufVxyXG5cclxuaW9uLWhlYWRlciBpb24tdG9vbGJhciBpb24tdGl0bGUgLmVuZCB7XHJcblx0Y29sb3I6IHZhcigtLXNlY29uZGFyeSk7XHJcblx0bGV0dGVyLXNwYWNpbmc6IC41cHg7XHJcbn1cclxuXHJcbi5iYW5uZXIge1xyXG5cdHBhZGRpbmctdG9wOiAyNiU7XHJcblx0cGFkZGluZy1ib3R0b206IDI2JTtcclxuXHJcblx0aW1nIHtcclxuXHRcdHdpZHRoOiAxMzlweDtcclxuXHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0fVxyXG59XHJcblxyXG5oMiB7XHJcblx0bWFyZ2luOiAwO1xyXG5cdGNvbG9yOiB2YXIoLS1wcmltYXJ5LXRleHQpO1xyXG5cdGZvbnQtc2l6ZTogMS4xcmVtO1xyXG5cdGZvbnQtd2VpZ2h0OiA1MDA7XHJcblx0bWFyZ2luLWJvdHRvbTogNDBweDtcclxufVxyXG5cclxuLm9yX2NvbnRpbnVlX3dpdGgge1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG5cdG1hcmdpbjogMCBhdXRvO1xyXG5cdHdpZHRoOiBjYWxjKDEwMCUgLSA0M3B4KTtcclxuXHRtYXJnaW4tdG9wOiAxNSU7XHJcblxyXG5cdGgyIHtcclxuXHRcdG1hcmdpbi1ib3R0b206IDIzcHg7XHJcblx0fVxyXG5cclxuXHRpb24tcm93IHtcclxuXHRcdG1hcmdpbjogMCAtNXB4O1xyXG5cclxuXHRcdGlvbi1jb2wge1xyXG5cdFx0XHQuYnV0dG9uLmJ0bi5idXR0b24tb3V0bGluZSB7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDQwMDtcclxuXHRcdFx0XHRmb250LXNpemU6IC45NXJlbTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/sign-in/sign-in.page.ts":
/*!*****************************************!*\
  !*** ./src/app/sign-in/sign-in.page.ts ***!
  \*****************************************/
/*! exports provided: SignInPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignInPage", function() { return SignInPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




let SignInPage = class SignInPage {
    constructor(route, navCtrl) {
        this.route = route;
        this.navCtrl = navCtrl;
    }
    ngOnInit() {
    }
    goTosignup() {
        this.route.navigate(['./sign-up']);
    }
    goTohome() {
        this.navCtrl.navigateRoot(['./home']);
    }
};
SignInPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
];
SignInPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-sign-in',
        template: __webpack_require__(/*! raw-loader!./sign-in.page.html */ "./node_modules/raw-loader/index.js!./src/app/sign-in/sign-in.page.html"),
        styles: [__webpack_require__(/*! ./sign-in.page.scss */ "./src/app/sign-in/sign-in.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
], SignInPage);



/***/ })

}]);
//# sourceMappingURL=sign-in-sign-in-module-es2015.js.map