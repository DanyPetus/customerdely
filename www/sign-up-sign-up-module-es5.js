(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["sign-up-sign-up-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/sign-up/sign-up.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/sign-up/sign-up.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>Sign up</ion-title>\n\t</ion-toolbar>\n\n</ion-header>\n \n<ion-content class=\"bg_img\" fullscreen>\n\t<div class=\"form\">\n\t\t<div class=\"profile_box d-flex\">\n\t\t\t<div class=\"img_box center_img\">\n\t\t\t\t<img src=\"assets/img/profile.png\" class=\"crop_img\">\n\t\t\t</div>\n\t\t\t<h2 class=\"d-flex\">\n\t\t\t\t<ion-icon text-center class=\"zmdi zmdi-camera\"></ion-icon>Add profile picture\n\t\t\t</h2>\n\t\t</div>\n\n\t\t<div class=\"list\">\n\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Full Name</ion-label>\n\t\t\t\t<ion-input type=\"email\" value=\"Samantha Smith\"></ion-input>\n\t\t\t</ion-item>\n\t\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Email Address</ion-label>\n\t\t\t\t<ion-input type=\"email\" value=\"samanthasmith@mail.com\"></ion-input>\n\t\t\t</ion-item>\n\t\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Phone Number</ion-label>\n\t\t\t\t<ion-input type=\"text\" value=\"+1987 654 3210\"></ion-input>\n\t\t\t</ion-item>\n\t\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Create Password</ion-label>\n\t\t\t\t<ion-input type=\"password\" value=\"111111\"></ion-input>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<ion-label position=\"floating\">Confirm Password</ion-label>\n\t\t\t\t<ion-input type=\"password\" value=\"123456\"></ion-input> \n\t\t\t</ion-item>\n\t\t\t<ion-button expand=\"block\" class=\"btn ion-text-uppercase\" (click)=\"goTohome()\">Sign Up</ion-button>\n\t\t</div>\n\t</div>\n</ion-content>"

/***/ }),

/***/ "./src/app/sign-up/sign-up.module.ts":
/*!*******************************************!*\
  !*** ./src/app/sign-up/sign-up.module.ts ***!
  \*******************************************/
/*! exports provided: SignUpPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPageModule", function() { return SignUpPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _sign_up_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sign-up.page */ "./src/app/sign-up/sign-up.page.ts");







var routes = [
    {
        path: '',
        component: _sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]
    }
];
var SignUpPageModule = /** @class */ (function () {
    function SignUpPageModule() {
    }
    SignUpPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_sign_up_page__WEBPACK_IMPORTED_MODULE_6__["SignUpPage"]]
        })
    ], SignUpPageModule);
    return SignUpPageModule;
}());



/***/ }),

/***/ "./src/app/sign-up/sign-up.page.scss":
/*!*******************************************!*\
  !*** ./src/app/sign-up/sign-up.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat; }\n\n.form .profile_box {\n  margin-bottom: 24px;\n  margin-top: 10px; }\n\n.form .profile_box .img_box {\n    min-width: 105px;\n    height: 105px;\n    border-radius: 50%; }\n\n.form .profile_box h2 {\n    margin: 0;\n    color: var(--white);\n    font-size: 1rem; }\n\n.form .profile_box h2 ion-icon {\n      color: var(--primary);\n      background: var(--secondary);\n      border-radius: 50%;\n      font-size: 1rem;\n      min-width: 23px;\n      height: 23px;\n      line-height: 23px;\n      margin: 0 13px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2lnbi11cC9FOlxcaW9uaWM0XFxmb29kbWFydFxcY3VzdG9tZXIvc3JjXFxhcHBcXHNpZ24tdXBcXHNpZ24tdXAucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWEsRUFBQTs7QUFJZDtFQUVFLG1CQUFtQjtFQUNuQixnQkFBZ0IsRUFBQTs7QUFIbEI7SUFNRyxnQkFBZ0I7SUFDaEIsYUFBYTtJQUNiLGtCQUFrQixFQUFBOztBQVJyQjtJQWFHLFNBQVM7SUFDVCxtQkFBbUI7SUFDbkIsZUFBZSxFQUFBOztBQWZsQjtNQWtCSSxxQkFBcUI7TUFDckIsNEJBQTRCO01BQzVCLGtCQUFrQjtNQUNsQixlQUFlO01BQ2YsZUFBZTtNQUNmLFlBQVk7TUFDWixpQkFBaUI7TUFDakIsY0FBYyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvc2lnbi11cC9zaWduLXVwLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50LmJnX2ltZyB7XHJcblx0LS1iYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZy9iZy5wbmcpIDAgMC8xMDAlIDEwMCUgbm8tcmVwZWF0O1xyXG5cdC8vLS1wYWRkaW5nLXRvcDogNTBweDtcclxufVxyXG5cclxuLmZvcm0ge1xyXG5cdC5wcm9maWxlX2JveCB7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAyNHB4O1xyXG5cdFx0bWFyZ2luLXRvcDogMTBweDtcclxuXHJcblx0XHQuaW1nX2JveCB7XHJcblx0XHRcdG1pbi13aWR0aDogMTA1cHg7XHJcblx0XHRcdGhlaWdodDogMTA1cHg7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHJcblx0XHR9XHJcblxyXG5cdFx0aDIge1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS13aGl0ZSk7XHJcblx0XHRcdGZvbnQtc2l6ZTogMXJlbTtcclxuXHJcblx0XHRcdGlvbi1pY29uIHtcclxuXHRcdFx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogdmFyKC0tc2Vjb25kYXJ5KTtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiA1MCU7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxcmVtO1xyXG5cdFx0XHRcdG1pbi13aWR0aDogMjNweDtcclxuXHRcdFx0XHRoZWlnaHQ6IDIzcHg7XHJcblx0XHRcdFx0bGluZS1oZWlnaHQ6IDIzcHg7XHJcblx0XHRcdFx0bWFyZ2luOiAwIDEzcHg7XHJcblxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/sign-up/sign-up.page.ts":
/*!*****************************************!*\
  !*** ./src/app/sign-up/sign-up.page.ts ***!
  \*****************************************/
/*! exports provided: SignUpPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignUpPage", function() { return SignUpPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");




var SignUpPage = /** @class */ (function () {
    function SignUpPage(route, navCtrl) {
        this.route = route;
        this.navCtrl = navCtrl;
    }
    SignUpPage.prototype.ngOnInit = function () {
    };
    SignUpPage.prototype.goTohome = function () {
        this.navCtrl.navigateRoot(['./home']);
    };
    SignUpPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] }
    ]; };
    SignUpPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-sign-up',
            template: __webpack_require__(/*! raw-loader!./sign-up.page.html */ "./node_modules/raw-loader/index.js!./src/app/sign-up/sign-up.page.html"),
            styles: [__webpack_require__(/*! ./sign-up.page.scss */ "./src/app/sign-up/sign-up.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]])
    ], SignUpPage);
    return SignUpPage;
}());



/***/ })

}]);
//# sourceMappingURL=sign-up-sign-up-module-es5.js.map