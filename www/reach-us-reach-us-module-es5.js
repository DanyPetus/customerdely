(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reach-us-reach-us-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/reach-us/reach-us.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/reach-us/reach-us.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\" class=\"dark_icon\">\n\t\t\t<ion-menu-button></ion-menu-button>\n\t\t</ion-buttons>\n\t\t<ion-title></ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg_img\"  fullscreen>\n\t<ion-list lines=\"none\">\n\t\t<div class=\"banner\">\n\t\t\t<div class=\"map\">\n\t\t\t\t<img src=\"assets/img/map.png\">\n\t\t\t</div>\n\t\t\t<ion-icon class=\"zmdi zmdi-navigation ion-text-center\"></ion-icon>\n\t\t\t<ion-card>\n\t\t\t\t<h2>We're here</h2>\n\t\t\t\t<p>105, Sliver Perl Housings, Golden Tower<br> Near City Part, Washington DC, <br> United States of America</p>\n\t\t\t</ion-card>\n\t\t</div>\n\t\t<ion-card>\n\t\t\t<h2>Contact us</h2>\n\t\t\t<div class=\"contacts d-flex\">\n\t\t\t\t<div class=\"text_box\">\n\t\t\t\t\t<h3>Call us</h3>\n\t\t\t\t\t<p>+1 987 654 3210</p>\n\t\t\t\t</div>\n\t\t\t\t<ion-icon class=\"zmdi zmdi-phone end ion-text-center\"></ion-icon>\n\t\t\t</div>\n\n\t\t\t<div class=\"contacts d-flex\">\n\t\t\t\t<div class=\"text_box\">\n\t\t\t\t\t<h3>Mail us</h3>\n\t\t\t\t\t<p>help@foodmart.com</p>\n\t\t\t\t</div>\n\t\t\t\t<ion-icon class=\"zmdi zmdi-email end ion-text-center\"></ion-icon>\n\t\t\t</div>\n\t\t</ion-card>\n\n\t\t<ion-card class=\"form\">\n\t\t\t<h2>Wrire us</h2>\n\t\t\t<ion-item>\n\t\t\t\t<ion-textarea  rows=\"1\" placeholder=\"Write your message or feedback\"></ion-textarea>\n\t\t\t</ion-item>\n\t\t\t<ion-button expand=\"full\" no class=\"btn ion-text-uppercase ion-no-margin\">\n\t\t\t\tSubmit\n\t\t\t</ion-button>\n\t\t</ion-card>\n\t</ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/reach-us/reach-us.module.ts":
/*!*********************************************!*\
  !*** ./src/app/reach-us/reach-us.module.ts ***!
  \*********************************************/
/*! exports provided: ReachUsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReachUsPageModule", function() { return ReachUsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _reach_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reach-us.page */ "./src/app/reach-us/reach-us.page.ts");







var routes = [
    {
        path: '',
        component: _reach_us_page__WEBPACK_IMPORTED_MODULE_6__["ReachUsPage"]
    }
];
var ReachUsPageModule = /** @class */ (function () {
    function ReachUsPageModule() {
    }
    ReachUsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_reach_us_page__WEBPACK_IMPORTED_MODULE_6__["ReachUsPage"]]
        })
    ], ReachUsPageModule);
    return ReachUsPageModule;
}());



/***/ }),

/***/ "./src/app/reach-us/reach-us.page.scss":
/*!*********************************************!*\
  !*** ./src/app/reach-us/reach-us.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat;\n  --padding: 0 !important;\n  --padding-top: 0 !important; }\n\nion-list {\n  background: var(--transparent) !important;\n  padding: 0;\n  margin: 0;\n  position: relative; }\n\nion-list .banner {\n    position: relative;\n    overflow: hidden;\n    width: 100%;\n    height: 413px;\n    margin-bottom: 9px; }\n\nion-list .banner .map {\n      position: relative;\n      overflow: hidden;\n      width: 100%;\n      max-height: 320px; }\n\nion-list .banner ion-icon {\n      position: absolute;\n      top: calc(100% - 139px);\n      right: 38px;\n      z-index: 999; }\n\nion-list .banner ion-card {\n      position: absolute;\n      bottom: 0;\n      box-shadow: 0 9px 36px -3px rgba(0, 0, 0, 0.3);\n      margin-bottom: 0 !important;\n      left: 0;\n      right: 0; }\n\nion-list ion-icon {\n    color: var(--white);\n    border-radius: 5px;\n    min-width: 45px;\n    height: 45px;\n    max-width: 45px;\n    background: #eed406;\n    line-height: 45px;\n    font-size: 1.3rem; }\n\nion-list ion-card {\n    background: var(--white);\n    margin: 0 auto;\n    margin-bottom: 9px;\n    border-radius: 9px;\n    width: calc(100% - 28px);\n    padding: 22px 20px; }\n\nion-list ion-card h2 {\n      margin: 0;\n      color: var(--text-light);\n      font-size: 1rem;\n      font-weight: 500;\n      margin-bottom: 10px; }\n\nion-list ion-card p {\n      margin: 0;\n      color: var(--black);\n      font-weight: 400;\n      font-size: .9rem; }\n\nion-list ion-card .contacts {\n      margin-bottom: 15px; }\n\nion-list ion-card .contacts h3 {\n        margin: 0;\n        color: var(--text-dark);\n        font-size: 1rem;\n        font-weight: 700;\n        margin-bottom: 5px; }\n\nion-list ion-card .contacts:last-child {\n        margin: 0; }\n\nion-list ion-card.form {\n      padding-bottom: 0;\n      padding-left: 0;\n      padding-right: 0; }\n\nion-list ion-card.form h2 {\n        padding: 0 20px;\n        margin-bottom: 11px; }\n\nion-list ion-card.form ion-item {\n        padding: 0;\n        width: calc(100% - 40px);\n        margin: 0 auto;\n        margin-bottom: 20px;\n        border-radius: 0;\n        border-bottom: 1px solid var(--text-light) !important; }\n\nion-list ion-card.form ion-item ion-textarea {\n          border-bottom: none !important;\n          font-size: .9rem !important;\n          font-weight: 400;\n          --padding-bottom: 0px !important;\n          --padding-top: 15px !important; }\n\nion-list ion-card.form .button.btn {\n        font-size: 1.15rem;\n        height: 60px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmVhY2gtdXMvRTpcXGlvbmljNFxcZm9vZG1hcnRcXGN1c3RvbWVyL3NyY1xcYXBwXFxyZWFjaC11c1xccmVhY2gtdXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWE7RUFDYix1QkFBVTtFQUNWLDJCQUFjLEVBQUE7O0FBR2Y7RUFDQyx5Q0FBeUM7RUFDekMsVUFBVTtFQUNWLFNBQVM7RUFDVCxrQkFBa0IsRUFBQTs7QUFKbkI7SUFPRSxrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxhQUFhO0lBQ2Isa0JBQWtCLEVBQUE7O0FBWHBCO01BY0csa0JBQWtCO01BQ2xCLGdCQUFnQjtNQUNoQixXQUFXO01BQ1gsaUJBQWlCLEVBQUE7O0FBakJwQjtNQXFCRyxrQkFBa0I7TUFDbEIsdUJBQXVCO01BQ3ZCLFdBQVc7TUFDWCxZQUFZLEVBQUE7O0FBeEJmO01BNEJHLGtCQUFrQjtNQUNsQixTQUFTO01BQ1QsOENBQThDO01BQzlDLDJCQUEyQjtNQUMzQixPQUFPO01BQ1AsUUFBUSxFQUFBOztBQWpDWDtJQXNDRSxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixZQUFZO0lBQ1osZUFBZTtJQUVmLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsaUJBQWlCLEVBQUE7O0FBOUNuQjtJQWtERSx3QkFBd0I7SUFDeEIsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsd0JBQXdCO0lBQ3hCLGtCQUFrQixFQUFBOztBQXZEcEI7TUEyREcsU0FBUztNQUNULHdCQUF3QjtNQUN4QixlQUFlO01BQ2YsZ0JBQWdCO01BQ2hCLG1CQUFtQixFQUFBOztBQS9EdEI7TUFtRUcsU0FBUztNQUNULG1CQUFtQjtNQUNuQixnQkFBZ0I7TUFDaEIsZ0JBQWdCLEVBQUE7O0FBdEVuQjtNQTBFRyxtQkFBbUIsRUFBQTs7QUExRXRCO1FBNkVJLFNBQVM7UUFDVCx1QkFBdUI7UUFDdkIsZUFBZTtRQUNmLGdCQUFnQjtRQUNoQixrQkFBa0IsRUFBQTs7QUFqRnRCO1FBcUZJLFNBQVMsRUFBQTs7QUFyRmI7TUEwRkcsaUJBQWlCO01BQ2pCLGVBQWU7TUFDZixnQkFBZ0IsRUFBQTs7QUE1Rm5CO1FBK0ZJLGVBQWU7UUFDZixtQkFBbUIsRUFBQTs7QUFoR3ZCO1FBb0dJLFVBQVU7UUFDVix3QkFBd0I7UUFDeEIsY0FBYztRQUNkLG1CQUFtQjtRQUNuQixnQkFBZ0I7UUFDaEIscURBQXFELEVBQUE7O0FBekd6RDtVQTZHSyw4QkFBOEI7VUFDOUIsMkJBQTJCO1VBQzNCLGdCQUFnQjtVQUNoQixnQ0FBaUI7VUFDakIsOEJBQWMsRUFBQTs7QUFqSG5CO1FBc0hJLGtCQUFrQjtRQUNsQixZQUFZLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9yZWFjaC11cy9yZWFjaC11cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5iZ19pbWcge1xyXG5cdC0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcucG5nKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuXHQtLXBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuXHQtLXBhZGRpbmctdG9wOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlvbi1saXN0IHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHRwYWRkaW5nOiAwO1xyXG5cdG1hcmdpbjogMDtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG5cdC5iYW5uZXIge1xyXG5cdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0b3ZlcmZsb3c6IGhpZGRlbjtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiA0MTNweDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDlweDtcclxuXHJcblx0XHQubWFwIHtcclxuXHRcdFx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0bWF4LWhlaWdodDogMzIwcHg7XHJcblx0XHR9XHJcblxyXG5cdFx0aW9uLWljb24ge1xyXG5cdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdHRvcDogY2FsYygxMDAlIC0gMTM5cHgpO1xyXG5cdFx0XHRyaWdodDogMzhweDtcclxuXHRcdFx0ei1pbmRleDogOTk5O1xyXG5cdFx0fVxyXG5cclxuXHRcdGlvbi1jYXJkIHtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRib3R0b206IDA7XHJcblx0XHRcdGJveC1zaGFkb3c6IDAgOXB4IDM2cHggLTNweCByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDAgIWltcG9ydGFudDtcclxuXHRcdFx0bGVmdDogMDtcclxuXHRcdFx0cmlnaHQ6IDA7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRpb24taWNvbiB7XHJcblx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogNXB4O1xyXG5cdFx0bWluLXdpZHRoOiA0NXB4O1xyXG5cdFx0aGVpZ2h0OiA0NXB4O1xyXG5cdFx0bWF4LXdpZHRoOiA0NXB4O1xyXG5cdFx0Ly9iYWNrZ3JvdW5kOiB2YXIoLS1zZWNvbmRhcnkpO1xyXG5cdFx0YmFja2dyb3VuZDogI2VlZDQwNjtcclxuXHRcdGxpbmUtaGVpZ2h0OiA0NXB4O1xyXG5cdFx0Zm9udC1zaXplOiAxLjNyZW07XHJcblx0fVxyXG5cclxuXHRpb24tY2FyZCB7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0XHRtYXJnaW46IDAgYXV0bztcclxuXHRcdG1hcmdpbi1ib3R0b206IDlweDtcclxuXHRcdGJvcmRlci1yYWRpdXM6IDlweDtcclxuXHRcdHdpZHRoOiBjYWxjKDEwMCUgLSAyOHB4KTtcclxuXHRcdHBhZGRpbmc6IDIycHggMjBweDtcclxuXHJcblxyXG5cdFx0aDIge1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWxpZ2h0KTtcclxuXHRcdFx0Zm9udC1zaXplOiAxcmVtO1xyXG5cdFx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cdFx0fVxyXG5cclxuXHRcdHAge1xyXG5cdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdGNvbG9yOiB2YXIoLS1ibGFjayk7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA0MDA7XHJcblx0XHRcdGZvbnQtc2l6ZTogLjlyZW07XHJcblx0XHR9XHJcblxyXG5cdFx0LmNvbnRhY3RzIHtcclxuXHRcdFx0bWFyZ2luLWJvdHRvbTogMTVweDtcclxuXHJcblx0XHRcdGgzIHtcclxuXHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLXRleHQtZGFyayk7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxcmVtO1xyXG5cdFx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdFx0bWFyZ2luLWJvdHRvbTogNXB4O1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQmOmxhc3QtY2hpbGQge1xyXG5cdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdCYuZm9ybSB7XHJcblx0XHRcdHBhZGRpbmctYm90dG9tOiAwO1xyXG5cdFx0XHRwYWRkaW5nLWxlZnQ6IDA7XHJcblx0XHRcdHBhZGRpbmctcmlnaHQ6IDA7XHJcblxyXG5cdFx0XHRoMiB7XHJcblx0XHRcdFx0cGFkZGluZzogMCAyMHB4O1xyXG5cdFx0XHRcdG1hcmdpbi1ib3R0b206IDExcHg7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlvbi1pdGVtIHtcclxuXHRcdFx0XHRwYWRkaW5nOiAwO1xyXG5cdFx0XHRcdHdpZHRoOiBjYWxjKDEwMCUgLSA0MHB4KTtcclxuXHRcdFx0XHRtYXJnaW46IDAgYXV0bztcclxuXHRcdFx0XHRtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDA7XHJcblx0XHRcdFx0Ym9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLXRleHQtbGlnaHQpICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0Ly9oZWlnaHQ6IDI2cHg7XHJcblxyXG5cdFx0XHRcdGlvbi10ZXh0YXJlYSB7XHJcblx0XHRcdFx0XHRib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRmb250LXNpemU6IC45cmVtICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHRmb250LXdlaWdodDogNDAwO1xyXG5cdFx0XHRcdFx0LS1wYWRkaW5nLWJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0XHQtLXBhZGRpbmctdG9wOiAxNXB4ICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQuYnV0dG9uLmJ0biB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtOyBcclxuXHRcdFx0XHRoZWlnaHQ6IDYwcHg7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG59Il19 */"

/***/ }),

/***/ "./src/app/reach-us/reach-us.page.ts":
/*!*******************************************!*\
  !*** ./src/app/reach-us/reach-us.page.ts ***!
  \*******************************************/
/*! exports provided: ReachUsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReachUsPage", function() { return ReachUsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ReachUsPage = /** @class */ (function () {
    function ReachUsPage() {
    }
    ReachUsPage.prototype.ngOnInit = function () {
    };
    ReachUsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-reach-us',
            template: __webpack_require__(/*! raw-loader!./reach-us.page.html */ "./node_modules/raw-loader/index.js!./src/app/reach-us/reach-us.page.html"),
            styles: [__webpack_require__(/*! ./reach-us.page.scss */ "./src/app/reach-us/reach-us.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ReachUsPage);
    return ReachUsPage;
}());



/***/ })

}]);
//# sourceMappingURL=reach-us-reach-us-module-es5.js.map