(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["address-address-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/address/address.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/address/address.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-buttons slot=\"start\">\n\t\t\t<ion-back-button></ion-back-button>\n\t\t</ion-buttons>\n\t\t<ion-title>Select Delivery Address</ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg_img\" fullscreen>\n\t<ion-list lines=\"none\">\n\t\t<ion-radio-group>\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_container\">\n\t\t\t\t\t<div class=\"item_header d-flex\">\n\t\t\t\t\t\t<h2>Home</h2>\n\t\t\t\t\t\t<ion-radio value=\"1\" class=\"end\"></ion-radio>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"address\">\n\t\t\t\t\t\t<p>105, Sliver Perl Housings, Golden Tower<br> Near City Part, Washington DC, <br> United States of America</p>\n\t\t\t\t\t\t<p>+1 987 654 3210</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_container\">\n\t\t\t\t\t<div class=\"item_header d-flex\">\n\t\t\t\t\t\t<h2>Farm House</h2>\n\t\t\t\t\t\t<ion-radio value=\"2\" class=\"end\"></ion-radio>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"address\">\n\t\t\t\t\t\t<p> B 441, Old city town, Leminton Street<br> Near City Part, Washington DC, <br> United States of America</p>\n\t\t\t\t\t\t<p>+1 987 654 3210</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\n\t\t\t<ion-item>\n\t\t\t\t<div class=\"item_container\">\n\t\t\t\t\t<div class=\"item_header\">\n\t\t\t\t\t\t<h2 class=\"d-flex\">Add New Address <ion-icon class=\"end ion-text-end zmdi zmdi-plus-circle\"></ion-icon>\n\t\t\t\t\t\t</h2>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</ion-item>\n\t\t</ion-radio-group>\n\t</ion-list>\n</ion-content>\n\n<ion-footer>\n\t<div class=\"amount\">\n\t\t<h2 class=\"d-flex\">Amount Payable <span class=\"end\">$25.50</span></h2>\n\t</div>\n\n\t<ion-button expand=\"full\" no class=\"btn ion-text-uppercase ion-no-margin\" (click)=\"continue()\">\n\t\tProceed To Checkout\n\t\t<ion-icon class=\"zmdi zmdi-chevron-right ion-text-end\"></ion-icon>\n\t</ion-button>\n</ion-footer>"

/***/ }),

/***/ "./src/app/address/address.module.ts":
/*!*******************************************!*\
  !*** ./src/app/address/address.module.ts ***!
  \*******************************************/
/*! exports provided: AddressPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPageModule", function() { return AddressPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _address_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./address.page */ "./src/app/address/address.page.ts");







const routes = [
    {
        path: '',
        component: _address_page__WEBPACK_IMPORTED_MODULE_6__["AddressPage"]
    }
];
let AddressPageModule = class AddressPageModule {
};
AddressPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_address_page__WEBPACK_IMPORTED_MODULE_6__["AddressPage"]]
    })
], AddressPageModule);



/***/ }),

/***/ "./src/app/address/address.page.scss":
/*!*******************************************!*\
  !*** ./src/app/address/address.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat;\n  --padding: 0 !important; }\n  ion-content.bg_img::before {\n    content: '';\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background: var(--primary);\n    opacity: .2;\n    z-index: 1; }\n  ion-header {\n  -webkit-backdrop-filter: saturate(120%) blur(10px);\n          backdrop-filter: saturate(120%) blur(10px);\n  margin-bottom: 8px; }\n  ion-header::before {\n    content: '';\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background: var(--primary);\n    opacity: .2; }\n  ion-list {\n  background: var(--transparent) !important;\n  margin: 0;\n  padding: 0;\n  margin-top: 10px;\n  overflow: hidden;\n  position: relative;\n  z-index: 999; }\n  ion-list ion-item {\n    --inner-padding-end: 0px;\n    --inner-min-height: unset !important;\n    min-height: unset;\n    --padding-start: 0;\n    --background: var(--transparent);\n    width: calc(100% - 30px);\n    margin: 0 auto;\n    overflow: hidden;\n    border-radius: 9px;\n    margin-bottom: 15px; }\n  ion-list ion-item .item_container {\n      position: relative;\n      width: 100%;\n      overflow: hidden; }\n  ion-list ion-item .item_container::before {\n        content: '';\n        position: absolute;\n        top: 0;\n        left: 0;\n        width: 100%;\n        height: 100%;\n        background: var(--dark);\n        opacity: .8; }\n  ion-list ion-item .item_container .item_header {\n        position: relative;\n        z-index: 99;\n        padding: 15px 15px; }\n  ion-list ion-item .item_container .item_header h2 {\n          margin: 0;\n          color: var(--white);\n          font-size: 1.15rem; }\n  ion-list ion-item .item_container .item_header h2 ion-icon {\n            color: var(--secondary);\n            font-size: 1.4rem; }\n  ion-list ion-item .item_container .item_header ion-radio {\n          margin-top: 0;\n          margin-bottom: 0;\n          --color-checked: var(--secondary); }\n  ion-list ion-item .item_container .address {\n        background: var(--white);\n        width: 100%;\n        position: relative;\n        overflow-x: 99;\n        border-radius: 9px;\n        padding: 13px 15px;\n        padding-bottom: 3px; }\n  ion-list ion-item .item_container .address p {\n          margin: 0;\n          color: var(--text-dark);\n          font-size: .96rem;\n          margin-bottom: 14px; }\n  ion-footer {\n  background: var(--white);\n  border-radius: 10px 10px 0 0px;\n  padding-top: 15px; }\n  ion-footer .amount h2 {\n    margin: 0;\n    color: var(--primary);\n    font-weight: 700;\n    font-size: 1.2rem;\n    letter-spacing: 0;\n    width: calc(100% - 48px);\n    margin: 0 auto;\n    margin-bottom: 11px; }\n  ion-footer .amount h2 span {\n      color: #000; }\n  ion-footer .button.btn {\n    font-size: 1.15rem;\n    font-weight: 800;\n    height: 66px; }\n  ion-footer .button.btn ion-icon {\n      font-size: 2rem;\n      min-width: 41px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWRkcmVzcy9FOlxcaW9uaWM0XFxmb29kbWFydFxcY3VzdG9tZXIvc3JjXFxhcHBcXGFkZHJlc3NcXGFkZHJlc3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWE7RUFDYix1QkFBVSxFQUFBO0VBRlg7SUFNRSxXQUFXO0lBQ1gsa0JBQWtCO0lBQ2xCLE1BQU07SUFDTixPQUFPO0lBQ1AsV0FBVztJQUNYLFlBQVk7SUFDWiwwQkFBMEI7SUFDMUIsV0FBVztJQUNYLFVBQVUsRUFBQTtFQUtaO0VBQ0Msa0RBQTBDO1VBQTFDLDBDQUEwQztFQUMxQyxrQkFBa0IsRUFBQTtFQUZuQjtJQUtFLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCxXQUFXO0lBQ1gsWUFBWTtJQUNaLDBCQUEwQjtJQUMxQixXQUFXLEVBQUE7RUFLYjtFQUNDLHlDQUF5QztFQUN6QyxTQUFTO0VBQ1QsVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDakIsa0JBQWtCO0VBQ2pCLFlBQVksRUFBQTtFQVBiO0lBVUUsd0JBQW9CO0lBQ3BCLG9DQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsa0JBQWdCO0lBQ2hCLGdDQUFhO0lBQ2Isd0JBQXdCO0lBQ3hCLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQixFQUFBO0VBbkJyQjtNQXNCRyxrQkFBa0I7TUFDbEIsV0FBVztNQUNYLGdCQUFnQixFQUFBO0VBeEJuQjtRQTJCSSxXQUFXO1FBQ1gsa0JBQWtCO1FBQ2xCLE1BQU07UUFDTixPQUFPO1FBQ1AsV0FBVztRQUNYLFlBQVk7UUFDWix1QkFBdUI7UUFDdkIsV0FBVyxFQUFBO0VBbENmO1FBdUNJLGtCQUFrQjtRQUNsQixXQUFXO1FBQ1gsa0JBQWtCLEVBQUE7RUF6Q3RCO1VBNENLLFNBQVM7VUFDVCxtQkFBbUI7VUFDbkIsa0JBQWtCLEVBQUE7RUE5Q3ZCO1lBaURNLHVCQUF1QjtZQUN2QixpQkFBaUIsRUFBQTtFQWxEdkI7VUF1REssYUFBYTtVQUNiLGdCQUFnQjtVQUNoQixpQ0FBZ0IsRUFBQTtFQXpEckI7UUE4REksd0JBQXdCO1FBQ3hCLFdBQVc7UUFDWCxrQkFBa0I7UUFDbEIsY0FBYztRQUNkLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsbUJBQW1CLEVBQUE7RUFwRXZCO1VBdUVLLFNBQVM7VUFDVCx1QkFBdUI7VUFDdkIsaUJBQWlCO1VBQ2pCLG1CQUFtQixFQUFBO0VBU3hCO0VBQ0Msd0JBQXdCO0VBQ3hCLDhCQUE4QjtFQUM5QixpQkFBaUIsRUFBQTtFQUhsQjtJQU9HLFNBQVM7SUFDVCxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsd0JBQXdCO0lBQ3hCLGNBQWM7SUFDZCxtQkFBbUIsRUFBQTtFQWR0QjtNQWlCSSxXQUFXLEVBQUE7RUFqQmY7SUF1QkUsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZLEVBQUE7RUF6QmQ7TUE0QkcsZUFBZTtNQUNmLGVBQWUsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2FkZHJlc3MvYWRkcmVzcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5iZ19pbWcge1xyXG5cdC0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcucG5nKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuXHQtLXBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxuXHJcblx0Ly8tLXBhZGRpbmctdG9wOiAwICFpbXBvcnRhbnQ7XHJcblx0Jjo6YmVmb3JlIHtcclxuXHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0dG9wOiAwO1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiAxMDAlO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRvcGFjaXR5OiAuMjtcclxuXHRcdHotaW5kZXg6IDE7XHJcblxyXG5cdH1cclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcblx0YmFja2Ryb3AtZmlsdGVyOiBzYXR1cmF0ZSgxMjAlKSBibHVyKDEwcHgpO1xyXG5cdG1hcmdpbi1ib3R0b206IDhweDtcclxuXHJcblx0Jjo6YmVmb3JlIHtcclxuXHRcdGNvbnRlbnQ6ICcnO1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0dG9wOiAwO1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiAxMDAlO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRvcGFjaXR5OiAuMjtcclxuXHJcblx0fVxyXG59XHJcblxyXG5pb24tbGlzdCB7XHJcblx0YmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpICFpbXBvcnRhbnQ7XHJcblx0bWFyZ2luOiAwO1xyXG5cdHBhZGRpbmc6IDA7XHJcblx0bWFyZ2luLXRvcDogMTBweDtcclxuXHRvdmVyZmxvdzogaGlkZGVuO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0ei1pbmRleDogOTk5O1xyXG5cclxuXHRpb24taXRlbSB7XHJcblx0XHQtLWlubmVyLXBhZGRpbmctZW5kOiAwcHg7XHJcblx0XHQtLWlubmVyLW1pbi1oZWlnaHQ6IHVuc2V0ICFpbXBvcnRhbnQ7XHJcblx0XHRtaW4taGVpZ2h0OiB1bnNldDtcclxuXHRcdC0tcGFkZGluZy1zdGFydDogMDtcclxuXHRcdC0tYmFja2dyb3VuZDogdmFyKC0tdHJhbnNwYXJlbnQpO1xyXG5cdFx0d2lkdGg6IGNhbGMoMTAwJSAtIDMwcHgpO1xyXG5cdFx0bWFyZ2luOiAwIGF1dG87XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogOXB4O1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMTVweDtcclxuXHJcblx0XHQuaXRlbV9jb250YWluZXIge1xyXG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cclxuXHRcdFx0Jjo6YmVmb3JlIHtcclxuXHRcdFx0XHRjb250ZW50OiAnJztcclxuXHRcdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdFx0dG9wOiAwO1xyXG5cdFx0XHRcdGxlZnQ6IDA7XHJcblx0XHRcdFx0d2lkdGg6IDEwMCU7XHJcblx0XHRcdFx0aGVpZ2h0OiAxMDAlO1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLWRhcmspO1xyXG5cdFx0XHRcdG9wYWNpdHk6IC44O1xyXG5cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Lml0ZW1faGVhZGVyIHtcclxuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0ei1pbmRleDogOTk7XHJcblx0XHRcdFx0cGFkZGluZzogMTVweCAxNXB4O1xyXG5cclxuXHRcdFx0XHRoMiB7XHJcblx0XHRcdFx0XHRtYXJnaW46IDA7XHJcblx0XHRcdFx0XHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAxLjE1cmVtO1xyXG5cclxuXHRcdFx0XHRcdGlvbi1pY29uIHtcclxuXHRcdFx0XHRcdFx0Y29sb3I6IHZhcigtLXNlY29uZGFyeSk7XHJcblx0XHRcdFx0XHRcdGZvbnQtc2l6ZTogMS40cmVtO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0aW9uLXJhZGlvIHtcclxuXHRcdFx0XHRcdG1hcmdpbi10b3A6IDA7XHJcblx0XHRcdFx0XHRtYXJnaW4tYm90dG9tOiAwO1xyXG5cdFx0XHRcdFx0LS1jb2xvci1jaGVja2VkOiB2YXIoLS1zZWNvbmRhcnkpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0LmFkZHJlc3Mge1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6IHZhcigtLXdoaXRlKTtcclxuXHRcdFx0XHR3aWR0aDogMTAwJTtcclxuXHRcdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblx0XHRcdFx0b3ZlcmZsb3cteDogOTk7XHJcblx0XHRcdFx0Ym9yZGVyLXJhZGl1czogOXB4O1xyXG5cdFx0XHRcdHBhZGRpbmc6IDEzcHggMTVweDtcclxuXHRcdFx0XHRwYWRkaW5nLWJvdHRvbTogM3B4O1xyXG5cclxuXHRcdFx0XHRwIHtcclxuXHRcdFx0XHRcdG1hcmdpbjogMDtcclxuXHRcdFx0XHRcdGNvbG9yOiB2YXIoLS10ZXh0LWRhcmspO1xyXG5cdFx0XHRcdFx0Zm9udC1zaXplOiAuOTZyZW07XHJcblx0XHRcdFx0XHRtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcblxyXG5pb24tZm9vdGVyIHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0Ym9yZGVyLXJhZGl1czogMTBweCAxMHB4IDAgMHB4O1xyXG5cdHBhZGRpbmctdG9wOiAxNXB4O1xyXG5cclxuXHQuYW1vdW50IHtcclxuXHRcdGgyIHtcclxuXHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OiA3MDA7XHJcblx0XHRcdGZvbnQtc2l6ZTogMS4ycmVtO1xyXG5cdFx0XHRsZXR0ZXItc3BhY2luZzogMDtcclxuXHRcdFx0d2lkdGg6IGNhbGMoMTAwJSAtIDQ4cHgpO1xyXG5cdFx0XHRtYXJnaW46IDAgYXV0bztcclxuXHRcdFx0bWFyZ2luLWJvdHRvbTogMTFweDtcclxuXHJcblx0XHRcdHNwYW4ge1xyXG5cdFx0XHRcdGNvbG9yOiAjMDAwO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuYnV0dG9uLmJ0biB7XHJcblx0XHRmb250LXNpemU6IDEuMTVyZW07XHJcblx0XHRmb250LXdlaWdodDogODAwO1xyXG5cdFx0aGVpZ2h0OiA2NnB4O1xyXG5cclxuXHRcdGlvbi1pY29uIHtcclxuXHRcdFx0Zm9udC1zaXplOiAycmVtO1xyXG5cdFx0XHRtaW4td2lkdGg6IDQxcHg7XHJcblx0XHR9XHJcblx0fVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/address/address.page.ts":
/*!*****************************************!*\
  !*** ./src/app/address/address.page.ts ***!
  \*****************************************/
/*! exports provided: AddressPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddressPage", function() { return AddressPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



let AddressPage = class AddressPage {
    constructor(route) {
        this.route = route;
    }
    ngOnInit() {
    }
    continue() {
        this.route.navigate(['./payment']);
    }
};
AddressPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
AddressPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-address',
        template: __webpack_require__(/*! raw-loader!./address.page.html */ "./node_modules/raw-loader/index.js!./src/app/address/address.page.html"),
        styles: [__webpack_require__(/*! ./address.page.scss */ "./src/app/address/address.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], AddressPage);



/***/ })

}]);
//# sourceMappingURL=address-address-module-es2015.js.map