(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-confirmed-order-confirmed-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/order-confirmed/order-confirmed.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/order-confirmed/order-confirmed.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"bg_transparent\">\n\t<ion-toolbar>\n\t\t<ion-title></ion-title>\n\t</ion-toolbar>\n</ion-header>\n\n<ion-content class=\"bg_img\" fullscreen>\n\t<div class=\"banner ion-text-center\">\n\t\t<div class=\"banner_img\">\n\t\t\t<img src=\"assets/img/order_confirmed.png\">\n\t\t</div>\n\t\t<h1>Hey, Samantha</h1>\n\t\t<p>Your Order is confirmed !</p>\n\t</div>\n\t<ion-list lines=\"none\">\n\t\t<div class=\"order_details\">\n\t\t\t<h2 class=\"d-flex\">Your Order Number <span class=\"end\">CCDF145412</span></h2>\n\n\t\t\t<div class=\"delivery_address\">\n\t\t\t\t<h2 class=\"d-flex\">Delivery Address <span class=\"end\">Home</span></h2>\n\t\t\t\t<p>105, Sliver Perl Housings, Golden Tower<br> Near City Part, Washington DC, <br> United States of America</p>\n\t\t\t</div>\n\t\t\t<h2 class=\"d-flex\">Total Amount Paid <span class=\"end\">$ 25.50</span></h2>\n\t\t\t<h2 class=\"d-flex\">Amount Paid via<span class=\"end\">PayU Money</span></h2>\n\t\t</div>\n\t\t<ion-button expand=\"block\" class=\"btn ion-text-uppercase\" (click)=\"goTohome()\">Track Order</ion-button>\n\t</ion-list>\n</ion-content>"

/***/ }),

/***/ "./src/app/order-confirmed/order-confirmed.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/order-confirmed/order-confirmed.module.ts ***!
  \***********************************************************/
/*! exports provided: OrderConfirmedPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderConfirmedPageModule", function() { return OrderConfirmedPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _order_confirmed_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./order-confirmed.page */ "./src/app/order-confirmed/order-confirmed.page.ts");







const routes = [
    {
        path: '',
        component: _order_confirmed_page__WEBPACK_IMPORTED_MODULE_6__["OrderConfirmedPage"]
    }
];
let OrderConfirmedPageModule = class OrderConfirmedPageModule {
};
OrderConfirmedPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_order_confirmed_page__WEBPACK_IMPORTED_MODULE_6__["OrderConfirmedPage"]]
    })
], OrderConfirmedPageModule);



/***/ }),

/***/ "./src/app/order-confirmed/order-confirmed.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/order-confirmed/order-confirmed.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content.bg_img {\n  --background: url('bg.png') 0 0/100% 100% no-repeat; }\n  ion-content.bg_img::after {\n    content: '';\n    position: absolute;\n    top: -54px;\n    left: 0;\n    width: 100%;\n    height: 154%;\n    background: var(--primary);\n    opacity: .8; }\n  .banner {\n  color: var(--white);\n  padding: 27px 15px 50px 15px;\n  position: relative;\n  z-index: 99; }\n  .banner .banner_img {\n    margin: 0 auto;\n    width: 205px;\n    padding-bottom: 27px; }\n  .banner h1 {\n    margin: 0;\n    font-size: 2rem;\n    font-weight: 600;\n    margin-bottom: 14px; }\n  .banner p {\n    margin: 0;\n    font-size: 1.15rem;\n    font-weight: 400; }\n  ion-list {\n  background: var(--transparent) !important;\n  width: calc(100% - 30px);\n  margin: 0 auto;\n  position: relative;\n  z-index: 99; }\n  ion-list h2 {\n    font-size: 1rem;\n    color: var(--primary-text);\n    margin: 0;\n    margin-bottom: 20px; }\n  ion-list .order_details {\n    padding: 22px 22px;\n    background: var(--white);\n    border-radius: 9px;\n    margin-bottom: 10px; }\n  ion-list .order_details h2 {\n      padding: 0;\n      color: var(--text-light);\n      margin-bottom: 25px; }\n  ion-list .order_details h2 span {\n        color: var(--balck); }\n  ion-list .order_details h2:last-child {\n        margin-bottom: 5px; }\n  ion-list .order_details .delivery_address {\n      margin-bottom: 20px; }\n  ion-list .order_details .delivery_address h2 {\n        margin-bottom: 15px; }\n  ion-list .order_details .delivery_address p {\n        margin: 0;\n        font-size: .95rem;\n        color: var(--balck);\n        font-weight: 500; }\n  ion-list .button.btn {\n    height: 61px;\n    --border-radius: 9px;\n    font-size: 1.15rem;\n    font-weight: 800; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvb3JkZXItY29uZmlybWVkL0U6XFxpb25pYzRcXGZvb2RtYXJ0XFxjdXN0b21lci9zcmNcXGFwcFxcb3JkZXItY29uZmlybWVkXFxvcmRlci1jb25maXJtZWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsbURBQWEsRUFBQTtFQURkO0lBSUUsV0FBVztJQUNYLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsT0FBTztJQUNQLFdBQVc7SUFDWCxZQUFZO0lBQ1osMEJBQTBCO0lBQzFCLFdBQVcsRUFBQTtFQUtiO0VBQ0MsbUJBQW1CO0VBQ25CLDRCQUE0QjtFQUM1QixrQkFBa0I7RUFDbEIsV0FBVyxFQUFBO0VBSlo7SUFPRSxjQUFjO0lBQ2QsWUFBWTtJQUNaLG9CQUFvQixFQUFBO0VBVHRCO0lBYUUsU0FBUztJQUNULGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsbUJBQW1CLEVBQUE7RUFoQnJCO0lBb0JFLFNBQVM7SUFDVCxrQkFBa0I7SUFDbEIsZ0JBQWdCLEVBQUE7RUFNbEI7RUFDQyx5Q0FBeUM7RUFDekMsd0JBQXdCO0VBQ3hCLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsV0FBVyxFQUFBO0VBTFo7SUFRRSxlQUFlO0lBQ2YsMEJBQTBCO0lBQzFCLFNBQVM7SUFDVCxtQkFBbUIsRUFBQTtFQVhyQjtJQWVFLGtCQUFrQjtJQUNsQix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLG1CQUFtQixFQUFBO0VBbEJyQjtNQXFCRyxVQUFVO01BQ1Ysd0JBQXdCO01BQ3hCLG1CQUFtQixFQUFBO0VBdkJ0QjtRQTBCSSxtQkFBbUIsRUFBQTtFQTFCdkI7UUE4Qkksa0JBQWtCLEVBQUE7RUE5QnRCO01BbUNHLG1CQUFtQixFQUFBO0VBbkN0QjtRQXNDSSxtQkFBbUIsRUFBQTtFQXRDdkI7UUEwQ0ksU0FBUztRQUNULGlCQUFpQjtRQUNqQixtQkFBbUI7UUFDbkIsZ0JBQWdCLEVBQUE7RUE3Q3BCO0lBbURFLFlBQVk7SUFDWixvQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvb3JkZXItY29uZmlybWVkL29yZGVyLWNvbmZpcm1lZC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5iZ19pbWcge1xyXG5cdC0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcucG5nKSAwIDAvMTAwJSAxMDAlIG5vLXJlcGVhdDtcclxuXHJcblx0Jjo6YWZ0ZXIge1xyXG5cdFx0Y29udGVudDogJyc7XHJcblx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHR0b3A6IC01NHB4O1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHdpZHRoOiAxMDAlO1xyXG5cdFx0aGVpZ2h0OiAxNTQlO1xyXG5cdFx0YmFja2dyb3VuZDogdmFyKC0tcHJpbWFyeSk7XHJcblx0XHRvcGFjaXR5OiAuODtcclxuXHJcblx0fVxyXG59XHJcblxyXG4uYmFubmVyIHtcclxuXHRjb2xvcjogdmFyKC0td2hpdGUpO1xyXG5cdHBhZGRpbmc6IDI3cHggMTVweCA1MHB4IDE1cHg7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdHotaW5kZXg6IDk5O1xyXG5cclxuXHQuYmFubmVyX2ltZyB7XHJcblx0XHRtYXJnaW46IDAgYXV0bztcclxuXHRcdHdpZHRoOiAyMDVweDtcclxuXHRcdHBhZGRpbmctYm90dG9tOiAyN3B4O1xyXG5cdH1cclxuXHJcblx0aDEge1xyXG5cdFx0bWFyZ2luOiAwO1xyXG5cdFx0Zm9udC1zaXplOiAycmVtO1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDYwMDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDE0cHg7XHJcblx0fVxyXG5cclxuXHRwIHtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdGZvbnQtc2l6ZTogMS4xNXJlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiA0MDA7XHJcblx0fVxyXG5cclxufVxyXG5cclxuXHJcbmlvbi1saXN0IHtcclxuXHRiYWNrZ3JvdW5kOiB2YXIoLS10cmFuc3BhcmVudCkgIWltcG9ydGFudDtcclxuXHR3aWR0aDogY2FsYygxMDAlIC0gMzBweCk7XHJcblx0bWFyZ2luOiAwIGF1dG87XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdHotaW5kZXg6IDk5O1xyXG5cclxuXHRoMiB7XHJcblx0XHRmb250LXNpemU6IDFyZW07XHJcblx0XHRjb2xvcjogdmFyKC0tcHJpbWFyeS10ZXh0KTtcclxuXHRcdG1hcmdpbjogMDtcclxuXHRcdG1hcmdpbi1ib3R0b206IDIwcHg7XHJcblx0fVxyXG5cclxuXHQub3JkZXJfZGV0YWlscyB7XHJcblx0XHRwYWRkaW5nOiAyMnB4IDIycHg7XHJcblx0XHRiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XHJcblx0XHRib3JkZXItcmFkaXVzOiA5cHg7XHJcblx0XHRtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG5cclxuXHRcdGgyIHtcclxuXHRcdFx0cGFkZGluZzogMDtcclxuXHRcdFx0Y29sb3I6IHZhcigtLXRleHQtbGlnaHQpO1xyXG5cdFx0XHRtYXJnaW4tYm90dG9tOiAyNXB4O1xyXG5cclxuXHRcdFx0c3BhbiB7XHJcblx0XHRcdFx0Y29sb3I6IHZhcigtLWJhbGNrKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0JjpsYXN0LWNoaWxkIHtcclxuXHRcdFx0XHRtYXJnaW4tYm90dG9tOiA1cHg7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHQuZGVsaXZlcnlfYWRkcmVzcyB7XHJcblx0XHRcdG1hcmdpbi1ib3R0b206IDIwcHg7XHJcblxyXG5cdFx0XHRoMiB7XHJcblx0XHRcdFx0bWFyZ2luLWJvdHRvbTogMTVweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0cCB7XHJcblx0XHRcdFx0bWFyZ2luOiAwO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogLjk1cmVtO1xyXG5cdFx0XHRcdGNvbG9yOiB2YXIoLS1iYWxjayk7XHJcblx0XHRcdFx0Zm9udC13ZWlnaHQ6IDUwMDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0LmJ1dHRvbi5idG4ge1xyXG5cdFx0aGVpZ2h0OiA2MXB4O1xyXG5cdFx0LS1ib3JkZXItcmFkaXVzOiA5cHg7XHJcblx0XHRmb250LXNpemU6IDEuMTVyZW07XHJcblx0XHRmb250LXdlaWdodDogODAwO1xyXG5cdH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/order-confirmed/order-confirmed.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/order-confirmed/order-confirmed.page.ts ***!
  \*********************************************************/
/*! exports provided: OrderConfirmedPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderConfirmedPage", function() { return OrderConfirmedPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let OrderConfirmedPage = class OrderConfirmedPage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ngOnInit() {
    }
    goTohome() {
        this.navCtrl.navigateRoot(['./home']);
    }
};
OrderConfirmedPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
OrderConfirmedPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-order-confirmed',
        template: __webpack_require__(/*! raw-loader!./order-confirmed.page.html */ "./node_modules/raw-loader/index.js!./src/app/order-confirmed/order-confirmed.page.html"),
        styles: [__webpack_require__(/*! ./order-confirmed.page.scss */ "./src/app/order-confirmed/order-confirmed.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
], OrderConfirmedPage);



/***/ })

}]);
//# sourceMappingURL=order-confirmed-order-confirmed-module-es2015.js.map