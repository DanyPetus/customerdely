import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  { path: 'sign-in', loadChildren: './sign-in/sign-in.module#SignInPageModule' },
  { path: 'sign-up', loadChildren: './sign-up/sign-up.module#SignUpPageModule' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'map-view', loadChildren: './map-view/map-view.module#MapViewPageModule' },
  { path: 'filters', loadChildren: './filters/filters.module#FiltersPageModule' },
  { path: 'restaurants', loadChildren: './restaurants/restaurants.module#RestaurantsPageModule' },
  { path: 'restro-info', loadChildren: './restro-info/restro-info.module#RestroInfoPageModule' },
  { path: 'items', loadChildren: './items/items.module#ItemsPageModule' },
  { path: 'cart', loadChildren: './cart/cart.module#CartPageModule' },
  { path: 'address', loadChildren: './address/address.module#AddressPageModule' },
  { path: 'payment', loadChildren: './payment/payment.module#PaymentPageModule' },
  { path: 'order-confirmed', loadChildren: './order-confirmed/order-confirmed.module#OrderConfirmedPageModule' },
  { path: 'my-orders', loadChildren: './my-orders/my-orders.module#MyOrdersPageModule' },
  { path: 'order-info', loadChildren: './order-info/order-info.module#OrderInfoPageModule' },
  { path: 'my-profle', loadChildren: './my-profle/my-profle.module#MyProflePageModule' },
  { path: 'reach-us', loadChildren: './reach-us/reach-us.module#ReachUsPageModule' },
  { path: 'conditions', loadChildren: './conditions/conditions.module#ConditionsPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
