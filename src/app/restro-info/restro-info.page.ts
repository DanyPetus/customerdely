import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-restro-info',
  templateUrl: './restro-info.page.html',
  styleUrls: ['./restro-info.page.scss'],
})
export class RestroInfoPage implements OnInit {

  constructor(private route: Router) { }

  ngOnInit() {
  }
	

 items() {
    this.route.navigate(['./items']);
  } 

}
