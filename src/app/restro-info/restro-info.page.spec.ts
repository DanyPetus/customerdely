import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RestroInfoPage } from './restro-info.page';

describe('RestroInfoPage', () => {
  let component: RestroInfoPage;
  let fixture: ComponentFixture<RestroInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RestroInfoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestroInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
