import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyProflePage } from './my-profle.page';

describe('MyProflePage', () => {
  let component: MyProflePage;
  let fixture: ComponentFixture<MyProflePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyProflePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyProflePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
