import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReachUsPage } from './reach-us.page';

describe('ReachUsPage', () => {
  let component: ReachUsPage;
  let fixture: ComponentFixture<ReachUsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReachUsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReachUsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
