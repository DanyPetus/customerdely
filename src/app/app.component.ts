import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home', 
	  image: 'assets/img/ic_home.png', 
      url: '/home',
		
    },
	{
      title: 'My Profile', 
	  image: 'assets/img/ic_my_profile.png', 
      url: '/my-profle',
    },
	  
	 {
      title: 'Favorites', 
	  image: 'assets/img/ic_favorites.png', 
      url: '/',
    },
	{
      title: 'My Orders', 
	  image: 'assets/img/ic_my_order.png', 
      url: '/my-orders',
    },
	{
      title: 'Terms & Conditions', 
	  image: 'assets/img/ic_t_c.png', 
      url: '/conditions',
    },
	  
	 {
      title: 'Reach us', 
      image: 'assets/img/ic_reach_us.png', 
      url: '/reach-us',
    },
  
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar, 
    private navCtrl: NavController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide(); 
    });
  }
	logOut() { 
	  this.navCtrl.navigateRoot(['./sign-in']);
  }
}

