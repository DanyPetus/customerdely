import { Component } from '@angular/core';
import { Router } from '@angular/router';  
import { ModalController } from '@ionic/angular';
import { FiltersPage } from '../Filters/filters.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private route: Router, private modalController: ModalController) {}
	
 
  goTorestaurants() {
    this.route.navigate(['./restaurants']);
  }  
 
	async filter(){
    const modal = await this.modalController.create({
      component: FiltersPage
  });
  await modal.present(); 
}

 restroDetails() {
    this.route.navigate(['./restro-info']);
  } 
	
  map() {
    this.route.navigate(['./map-view']);
  }

} 